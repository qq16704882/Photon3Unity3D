﻿using Photon.SocketServer.Numeric;
using System;
using System.Security.Cryptography;

namespace Photon.SocketServer.Security
{
    internal class DiffieHellmanCryptoProvider : IDisposable
    {
        // Fields
        private Rijndael crypto;
        private readonly BigInteger prime = new BigInteger(OakleyGroups.OakleyPrime768);
        private static readonly BigInteger primeRoot = new BigInteger((long)OakleyGroups.Generator);

        //<summary>
        //Gets the public key that can be used by another DiffieHellmanCryptoProvider object
        //to generate a shared secret agreement.
        //</summary>
        private readonly BigInteger publicKey;
        private readonly BigInteger secret;
        //<summary>
        // Gets the shared key that is used by the current instance for cryptographic operations.
        //</summary>
        private byte[] sharedKey;

        //<summary>
        //Initializes a new instance of the<see cref="T:Photon.SocketServer.Security.DiffieHellmanCryptoProvider"/> class.
        //</summary>
        public DiffieHellmanCryptoProvider()
        {
            this.secret = this.GenerateRandomSecret(160);
            this.publicKey = this.CalculatePublicKey();
        }

        private BigInteger CalculatePublicKey() =>
            primeRoot.ModPow(this.secret, this.prime);

        private BigInteger CalculateSharedKey(BigInteger otherPartyPublicKey) =>
            otherPartyPublicKey.ModPow(this.secret, this.prime);

        public byte[] Decrypt(byte[] data) =>
            this.Decrypt(data, 0, data.Length);

        public byte[] Decrypt(byte[] data, int offset, int count)
        {
            using (ICryptoTransform transform = this.crypto.CreateDecryptor())
            {
                return transform.TransformFinalBlock(data, offset, count);
            }
        }
        //<summary>
        //Derives the shared key is generated from the secret agreement between two parties,
        //given a byte array that contains the second party's public key. 
        //</summary>
        //<param name = "otherPartyPublicKey" >
        //The second party's public key.
        //</param>
        public void DeriveSharedKey(byte[] otherPartyPublicKey)
        {
            byte[] buffer;
            BigInteger integer = new BigInteger(otherPartyPublicKey);
            this.sharedKey = this.CalculateSharedKey(integer).GetBytes();
            using (SHA256 sha = new SHA256Managed())
            {
                buffer = sha.ComputeHash(this.SharedKey);
            }
            this.crypto = new RijndaelManaged();
            this.crypto.Key = buffer;
            this.crypto.IV = new byte[0x10];
            this.crypto.Padding = PaddingMode.PKCS7;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposing)
            {
            }
        }

        public byte[] Encrypt(byte[] data) =>
            this.Encrypt(data, 0, data.Length);

        public byte[] Encrypt(byte[] data, int offset, int count)
        {
            using (ICryptoTransform transform = this.crypto.CreateEncryptor())
            {
                return transform.TransformFinalBlock(data, offset, count);
            }
        }

        private BigInteger GenerateRandomSecret(int secretLength)
        {
            BigInteger integer;
            do
            {
                integer = BigInteger.GenerateRandom(secretLength);
            }
            while ((integer >= (this.prime - 1)) || (integer == 0));
            return integer;
        }

        // Properties
        public bool IsInitialized =>
            (this.crypto != null);

        public byte[] PublicKey =>
            this.publicKey.GetBytes();

        public byte[] SharedKey =>
            this.sharedKey;
    }
}