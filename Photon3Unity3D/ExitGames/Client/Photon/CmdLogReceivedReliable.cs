﻿namespace ExitGames.Client.Photon
{
    internal class CmdLogReceivedReliable : CmdLogItem
    {
        public int TimeSinceLastSend;
        public int TimeSinceLastSendAck;

        public CmdLogReceivedReliable(NCommand command, int timeInt, int rtt, int variance, int timeSinceLastSend, int timeSinceLastSendAck) : base(command, timeInt, rtt, variance)
        {
            this.TimeSinceLastSend = timeSinceLastSend;
            this.TimeSinceLastSendAck = timeSinceLastSendAck;
        }

        public override string ToString()
        {
            return $"Read reliable. {base.ToString()}  TimeSinceLastSend: {this.TimeSinceLastSend} TimeSinceLastSendAcks: {this.TimeSinceLastSendAck}";
        }
    }
}