﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ExitGames.Client.Photon
{
    public class DictionaryEntryEnumerator : IEnumerator<DictionaryEntry>, IDisposable, IEnumerator
    {
        private IDictionaryEnumerator enumerator;

        public DictionaryEntryEnumerator(IDictionaryEnumerator original)
        {
            this.enumerator = original;
        }

        public void Dispose()
        {
            this.enumerator = null;
        }

        public bool MoveNext() =>
            this.enumerator.MoveNext();

        public void Reset()
        {
            this.enumerator.Reset();
        }

        public DictionaryEntry Current =>
            (DictionaryEntry)this.enumerator.Current;

        public DictionaryEntry Entry =>
            this.enumerator.Entry;

        public object Key =>
            this.enumerator.Key;

        object IEnumerator.Current =>
            (DictionaryEntry)this.enumerator.Current;

        public object Value =>
            this.enumerator.Value;
    }
}
