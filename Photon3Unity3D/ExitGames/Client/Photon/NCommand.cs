﻿using System;

namespace ExitGames.Client.Photon
{
    //<summary> Internal class for "commands" - the package in which operations are sent.</summary>
    internal class NCommand : IComparable<NCommand>
    {
        internal int ackReceivedReliableSequenceNumber;
        internal int ackReceivedSentTime;
        internal const int CmdSizeAck = 20;
        internal const int CmdSizeConnect = 0x2c;
        internal const int CmdSizeDisconnect = 12;
        internal const int CmdSizeFragmentHeader = 0x20;
        internal const int CmdSizeMaxHeader = 0x24;
        internal const int CmdSizeMinimum = 12;
        internal const int CmdSizePing = 12;
        internal const int CmdSizeReliableHeader = 12;
        internal const int CmdSizeUnreliableHeader = 0x10;
        internal const int CmdSizeVerifyConnect = 0x2c;
        internal byte commandChannelID;
        internal byte commandFlags;
        internal byte commandSentCount;
        internal int commandSentTime;
        internal byte commandType;
        private byte[] completeCommand;
        internal const byte CT_ACK = 1;
        internal const byte CT_CONNECT = 2;
        internal const byte CT_DISCONNECT = 4;
        internal const byte CT_EG_SERVERTIME = 12;
        internal const byte CT_NONE = 0;
        internal const byte CT_PING = 5;
        internal const byte CT_SENDFRAGMENT = 8;
        internal const byte CT_SENDRELIABLE = 6;
        internal const byte CT_SENDUNRELIABLE = 7;
        internal const byte CT_VERIFYCONNECT = 3;
        internal const int FLAG_RELIABLE = 1;
        internal const int FLAG_UNSEQUENCED = 2;
        internal int fragmentCount;
        internal int fragmentNumber;
        internal int fragmentOffset;
        internal int fragmentsRemaining;
        internal const byte FV_RELIABLE = 1;
        internal const byte FV_UNRELIABLE = 0;
        internal const byte FV_UNRELIBALE_UNSEQUENCED = 2;
        internal const int HEADER_UDP_PACK_LENGTH = 12;
        internal byte[] Payload;
        internal int reliableSequenceNumber;
        internal byte reservedByte;
        internal int roundTripTimeout;
        internal int Size;
        internal int startSequenceNumber;
        internal int timeoutTime;
        internal int totalLength;
        internal int unreliableSequenceNumber;
        internal int unsequencedGroupNumber;

        //<summary>reads the command values (commandHeader and command-values) from incoming bytestream and populates the incoming command*</summary>
        internal NCommand(EnetPeer peer, byte[] inBuff, ref int readingOffset)
        {
            this.reservedByte = 4;
            this.commandType = inBuff[readingOffset++];
            this.commandChannelID = inBuff[readingOffset++];
            this.commandFlags = inBuff[readingOffset++];
            this.reservedByte = inBuff[readingOffset++];
            Protocol.Deserialize(out this.Size, inBuff, ref readingOffset);
            Protocol.Deserialize(out this.reliableSequenceNumber, inBuff, ref readingOffset);
            peer.bytesIn += this.Size;
            switch (this.commandType)
            {
                case CT_ACK:
                    Protocol.Deserialize(out this.ackReceivedReliableSequenceNumber, inBuff, ref readingOffset);
                    Protocol.Deserialize(out this.ackReceivedSentTime, inBuff, ref readingOffset);
                    break;

                case CT_VERIFYCONNECT:
                    short num;
                    Protocol.Deserialize(out num, inBuff, ref readingOffset);
                    readingOffset += 30;
                    if (peer.peerID == -1)
                    {
                        peer.peerID = num;
                    }
                    break;

                case CT_SENDRELIABLE:
                    this.Payload = new byte[this.Size - CmdSizeReliableHeader];
                    break;

                case CT_SENDUNRELIABLE:
                    Protocol.Deserialize(out this.unreliableSequenceNumber, inBuff, ref readingOffset);
                    this.Payload = new byte[this.Size - CmdSizeUnreliableHeader];
                    break;

                case CT_SENDFRAGMENT:
                    Protocol.Deserialize(out this.startSequenceNumber, inBuff, ref readingOffset);
                    Protocol.Deserialize(out this.fragmentCount, inBuff, ref readingOffset);
                    Protocol.Deserialize(out this.fragmentNumber, inBuff, ref readingOffset);
                    Protocol.Deserialize(out this.totalLength, inBuff, ref readingOffset);
                    Protocol.Deserialize(out this.fragmentOffset, inBuff, ref readingOffset);
                    this.Payload = new byte[this.Size - CmdSizeFragmentHeader];
                    this.fragmentsRemaining = this.fragmentCount;
                    break;
            }
            if (this.Payload != null)
            {
                Buffer.BlockCopy(inBuff, readingOffset, this.Payload, 0, this.Payload.Length);
                readingOffset += this.Payload.Length;
            }
        }
        //<summary>this variant does only create outgoing commands and increments . incoming ones are created from a DataInputStream</summary>
        internal NCommand(EnetPeer peer, byte commandType, byte[] payload, byte channel)
        {
            this.reservedByte = 4;
            this.commandType = commandType;
            this.commandFlags = 1;
            this.commandChannelID = channel;
            this.Payload = payload;
            this.Size = CmdSizeMinimum;
            switch (this.commandType)
            {
                case CT_ACK:
                    this.Size = CmdSizeAck;
                    this.commandFlags = 0;
                    break;

                case CT_CONNECT:
                    {
                        this.Size = CmdSizeVerifyConnect;
                        this.Payload = new byte[CmdSizeFragmentHeader];
                        this.Payload[0] = 0;
                        this.Payload[1] = 0;
                        int targetOffset = 2;
                        Protocol.Serialize((short)peer.mtu, this.Payload, ref targetOffset);
                        this.Payload[4] = 0;
                        this.Payload[5] = 0;
                        this.Payload[6] = 0x80;
                        this.Payload[7] = 0;
                        this.Payload[11] = peer.ChannelCount;
                        this.Payload[15] = 0;
                        this.Payload[0x13] = 0;
                        this.Payload[0x16] = 0x13;
                        this.Payload[0x17] = 0x88;
                        this.Payload[0x1b] = 2;
                        this.Payload[0x1f] = 2;
                        break;
                    }
                case CT_DISCONNECT:
                    this.Size = CmdSizeDisconnect;
                    if (peer.peerConnectionState != PeerBase.ConnectionStateValue.Connected)
                    {
                        this.commandFlags = 2;
                        if (peer.peerConnectionState == PeerBase.ConnectionStateValue.Zombie)
                        {
                            this.reservedByte = 2;
                        }
                    }
                    break;

                case CT_SENDRELIABLE:
                    this.Size = CmdSizeReliableHeader + payload.Length;
                    break;

                case CT_SENDUNRELIABLE:
                    this.Size = CmdSizeUnreliableHeader + payload.Length;
                    this.commandFlags = 0;
                    break;

                case CT_SENDFRAGMENT:
                    this.Size = CmdSizeFragmentHeader + payload.Length;
                    break;
            }
        }

        public int CompareTo(NCommand other)
        {
            if ((this.commandFlags & 1) != 0)
            {
                return (this.reliableSequenceNumber - other.reliableSequenceNumber);
            }
            return (this.unreliableSequenceNumber - other.unreliableSequenceNumber);
        }

        internal static NCommand CreateAck(EnetPeer peer, NCommand commandToAck, int sentTime)
        {
            byte[] target = new byte[8];
            int targetOffset = 0;
            Protocol.Serialize(commandToAck.reliableSequenceNumber, target, ref targetOffset);
            Protocol.Serialize(sentTime, target, ref targetOffset);
            return new NCommand(peer, CT_ACK, target, commandToAck.commandChannelID)
            {
                ackReceivedReliableSequenceNumber = commandToAck.reliableSequenceNumber,
                ackReceivedSentTime = sentTime
            };
        }

        internal byte[] Serialize()
        {
            if (this.completeCommand == null)
            {
                int count = (this.Payload == null) ? 0 : this.Payload.Length;
                int dstOffset = CmdSizeMinimum;
                if (this.commandType == CT_SENDUNRELIABLE)
                {
                    dstOffset = CmdSizeUnreliableHeader;
                }
                else if (this.commandType == CT_SENDFRAGMENT)
                {
                    dstOffset = CmdSizeFragmentHeader;
                }
                this.completeCommand = new byte[dstOffset + count];
                this.completeCommand[0] = this.commandType;
                this.completeCommand[1] = this.commandChannelID;
                this.completeCommand[2] = this.commandFlags;
                this.completeCommand[3] = this.reservedByte;
                int targetOffset = 4;
                Protocol.Serialize(this.completeCommand.Length, this.completeCommand, ref targetOffset);
                Protocol.Serialize(this.reliableSequenceNumber, this.completeCommand, ref targetOffset);
                if (this.commandType == CT_SENDUNRELIABLE)
                {
                    targetOffset = 12;
                    Protocol.Serialize(this.unreliableSequenceNumber, this.completeCommand, ref targetOffset);
                }
                else if (this.commandType == CT_SENDFRAGMENT)
                {
                    targetOffset = 12;
                    Protocol.Serialize(this.startSequenceNumber, this.completeCommand, ref targetOffset);
                    Protocol.Serialize(this.fragmentCount, this.completeCommand, ref targetOffset);
                    Protocol.Serialize(this.fragmentNumber, this.completeCommand, ref targetOffset);
                    Protocol.Serialize(this.totalLength, this.completeCommand, ref targetOffset);
                    Protocol.Serialize(this.fragmentOffset, this.completeCommand, ref targetOffset);
                }
                if (count > 0)
                {
                    Buffer.BlockCopy(this.Payload, 0, this.completeCommand, dstOffset, count);
                }
                this.Payload = null;
            }
            return this.completeCommand;
        }

        public override string ToString()
        {
            if (this.commandType == CT_ACK)
            {
                return string.Format("CMD({1} ack for c#:{0} s#/time {2}/{3})", new object[] { this.commandChannelID, this.commandType, this.ackReceivedReliableSequenceNumber, this.ackReceivedSentTime });
            }
            return string.Format("CMD({1} c#:{0} r/u: {2}/{3} st/r#/rt:{4}/{5}/{6})", new object[] { this.commandChannelID, this.commandType, this.reliableSequenceNumber, this.unreliableSequenceNumber, this.commandSentTime, this.commandSentCount, this.timeoutTime });
        }
    }
}
