﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ExitGames.Client.Photon
{
    //<summary>
    //Contains several(more or less) useful static methods, mostly used for debugging.
    //</summary>
    public class SupportClass
    {
        protected internal static IntegerMillisecondsDelegate IntegerMilliseconds = () => Environment.TickCount;
        //<summary>
        //Converts a byte-array to string (useful as debugging output).
        //Uses BitConverter.ToString(list) internally after a null-check of list.
        //</summary>
        //<param name = "list" > Byte - array to convert to string.</param>
        //<returns>
        //List of bytes as string.
        //</returns>
        public static string ByteArrayToString(byte[] list)
        {
            if (list == null)
            {
                return string.Empty;
            }
            return BitConverter.ToString(list);
        }

        public static uint CalculateCrc(byte[] buffer, int length)
        {
            uint maxValue = uint.MaxValue;
            uint num2 = 0xedb88320;//3988292384
            byte num3 = 0;
            for (int i = 0; i < length; i++)
            {
                num3 = buffer[i];
                maxValue ^= num3;
                for (int j = 0; j < 8; j++)
                {
                    if ((maxValue & 1) != 0)
                    {
                        maxValue = (maxValue >> 1) ^ num2;
                    }
                    else
                    {
                        maxValue = maxValue >> 1;
                    }
                }
            }
            return maxValue;
        }
        //<summary>
        //Creates a background thread that calls the passed function in 100ms intervals, as long as that returns true.
        //</summary>
        //<param name = "myThread" ></ param >
        public static void CallInBackground(Func<bool> myThread)
        {
            CallInBackground(myThread, 100);
        }
        //<summary>
        //Creates a background thread that calls the passed function in 100ms intervals, as long as that returns true.
        //</summary>
        //<param name = "myThread" ></ param >
        //< param name="millisecondsInterval">Milliseconds to sleep between calls of myThread.</param>
        public static void CallInBackground(Func<bool> myThread, int millisecondsInterval)
        {
            new Thread(() =>
            {
                while (myThread())
                {
                    Thread.Sleep(millisecondsInterval);
                }
            })
            { IsBackground = true }.Start();
        }
        //<summary>
        //This method returns a string, representing the content of the given IDictionary.
        //Returns "null" if parameter is null.
        //</summary>
        //<param name = "dictionary" >
        //IDictionary to return as string.
        //</param>
        //<returns>
        //The string representation of keys and values in IDictionary.
        //</returns>
        public static string DictionaryToString(IDictionary dictionary) =>
            DictionaryToString(dictionary, true);
        //<summary>
        //This method returns a string, representing the content of the given IDictionary.
        //Returns "null" if parameter is null.
        //</summary>
        //<param name = "dictionary" > IDictionary to return as string.</param>
        //<param name = "includeTypes" > </ param >
        public static string DictionaryToString(IDictionary dictionary, bool includeTypes)
        {
            if (dictionary == null)
            {
                return "null";
            }
            StringBuilder builder = new StringBuilder();
            builder.Append("{");
            foreach (object keyObject in dictionary.Keys)
            {
                Type type;
                string str;
                if (builder.Length > 1)
                {
                    builder.Append(", ");
                }
                if (dictionary[keyObject] == null)
                {
                    type = typeof(object);
                    str = "null";
                }
                else
                {
                    type = dictionary[keyObject].GetType();
                    str = dictionary[keyObject].ToString();
                }
                if ((typeof(IDictionary) == type) || (typeof(Hashtable) == type))
                {
                    str = DictionaryToString((IDictionary)dictionary[keyObject]);
                }
                if (typeof(string[]) == type)
                {
                    str = $"{{{string.Join(",", (string[])dictionary[keyObject])}}}";
                }
                if (includeTypes)
                {
                    builder.AppendFormat("({0}){1}=({2}){3}", new object[] { keyObject.GetType().Name, keyObject, type.Name, str });
                }
                else
                {
                    builder.AppendFormat("{0}={1}", keyObject, str);
                }
            }
            builder.Append("}");
            return builder.ToString();
        }

        public static List<MethodInfo> GetMethods(Type type, Type attribute)
        {
            List<MethodInfo> list = new List<MethodInfo>();
            if (type != null)
            {
                MethodInfo[] methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                foreach (MethodInfo info in methods)
                {
                    if ((attribute == null) || info.IsDefined(attribute, false))
                    {
                        list.Add(info);
                    }
                }
            }
            return list;
        }
        //<summary>
        //Gets the local machine's "milliseconds since start" value (precision is described in remarks).
        //</summary>
        //<remarks>
        //This method uses Environment.TickCount(cheap but with only 16ms precision).
        //PhotonPeer.LocalMsTimestampDelegate is available to set the delegate (unless already connected).
        //</remarks>
        //<returns>Fraction of the current time in Milliseconds(this is not a proper datetime timestamp).</returns>
        public static int GetTickCount() =>
            IntegerMilliseconds();

        [Obsolete("Use DictionaryToString() instead.")]
        public static string HashtableToString(Hashtable hash) =>
            DictionaryToString(hash);
        //<summary>
        //Inserts the number's value into the byte array, using Big-Endian order (a.k.a. Network-byte-order).
        //</summary>
        //<param name = "buffer" > Byte array to write into.</param>
        //<param name = "index" > Index of first position to write to.</param>
        //<param name = "number" > Number to write.</param>
        [Obsolete("Use Protocol.Serialize() instead.")]
        public static void NumberToByteArray(byte[] buffer, int index, short number)
        {
            Protocol.Serialize(number, buffer, ref index);
        }
        //<summary>
        //Inserts the number's value into the byte array, using Big-Endian order (a.k.a. Network-byte-order).
        //</summary>
        //<param name = "buffer" > Byte array to write into.</param>
        //<param name = "index" > Index of first position to write to.</param>
        //<param name = "number" > Number to write.</param>
        [Obsolete("Use Protocol.Serialize() instead.")]
        public static void NumberToByteArray(byte[] buffer, int index, int number)
        {
            Protocol.Serialize(number, buffer, ref index);
        }
        //<summary>
        //Writes the exception's stack trace to the received stream. Writes to: System.Diagnostics.Debug.
        //</summary>
        //<param name = "throwable" > Exception to obtain information from.</param>
        public static void WriteStackTrace(Exception throwable)
        {
            WriteStackTrace(throwable, null);
        }
        //<summary>
        //Writes the exception's stack trace to the received stream.
        //</summary>
        //<param name = "throwable" > Exception to obtain information from.</param>
        //<param name = "stream" > Output sream used to write to.</param>
        public static void WriteStackTrace(Exception throwable, TextWriter stream)
        {
            if (stream != null)
            {
                stream.WriteLine(throwable.ToString());
                stream.WriteLine(throwable.StackTrace);
                stream.Flush();
            }
            else
            {
                Debug.WriteLine(throwable.ToString());
                Debug.WriteLine(throwable.StackTrace);
            }
        }

        public delegate int IntegerMillisecondsDelegate();
        //<summary>
        //Class to wrap static access to the random.Next() call in a thread safe manner.
        //</summary>
        public class ThreadSafeRandom
        {
            private static readonly Random _r = new Random();

            public static int Next()
            {
                lock (_r)
                {
                    return _r.Next();
                }
            }
        }
    }
}
