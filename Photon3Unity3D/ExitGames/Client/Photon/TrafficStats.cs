﻿namespace ExitGames.Client.Photon
{
    public class TrafficStats
    {
        internal TrafficStats(int packageHeaderSize)
        {
            this.PackageHeaderSize = packageHeaderSize;
        }

        internal void CountControlCommand(int size)
        {
            this.ControlCommandBytes += size;
            this.ControlCommandCount++;
        }

        internal void CountFragmentOpCommand(int size)
        {
            this.FragmentCommandBytes += size;
            this.FragmentCommandCount++;
        }

        internal void CountReliableOpCommand(int size)
        {
            this.ReliableCommandBytes += size;
            this.ReliableCommandCount++;
        }

        internal void CountUnreliableOpCommand(int size)
        {
            this.UnreliableCommandBytes += size;
            this.UnreliableCommandCount++;
        }

        public override string ToString() =>
            $"TotalPacketBytes: {this.TotalPacketBytes} TotalCommandBytes: {this.TotalCommandBytes} TotalPacketCount: {this.TotalPacketCount} TotalCommandsInPackets: {this.TotalCommandsInPackets}";

        public int ControlCommandBytes { get; internal set; }

        public int ControlCommandCount { get; internal set; }

        public int FragmentCommandBytes { get; internal set; }

        public int FragmentCommandCount { get; internal set; }

        public int PackageHeaderSize { get; internal set; }

        public int ReliableCommandBytes { get; internal set; }

        public int ReliableCommandCount { get; internal set; }

        public int TimestampOfLastAck { get; set; }

        public int TimestampOfLastReliableCommand { get; set; }

        public int TotalCommandBytes =>
            this.ReliableCommandBytes + this.UnreliableCommandBytes + this.FragmentCommandBytes + this.ControlCommandBytes;

        public int TotalCommandCount =>
            this.ReliableCommandCount + this.UnreliableCommandCount + this.FragmentCommandCount + this.ControlCommandCount;

        public int TotalCommandsInPackets { get; internal set; }

        public int TotalPacketBytes =>
            this.TotalCommandBytes + this.TotalPacketCount * this.PackageHeaderSize;

        public int TotalPacketCount { get; internal set; }

        public int UnreliableCommandBytes { get; internal set; }

        public int UnreliableCommandCount { get; internal set; }
    }
}