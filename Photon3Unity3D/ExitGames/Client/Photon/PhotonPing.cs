﻿using System;

namespace ExitGames.Client.Photon
{
    public abstract class PhotonPing : IDisposable
    {
        public string DebugString = "";
        protected internal bool GotResult;
        protected internal byte[] PingBytes = new byte[] { 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0x7d, 0 };
        protected internal byte PingId;
        protected internal int PingLength = 13;
        public bool Successful;

        protected PhotonPing()
        {
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual bool Done()
        {
            throw new NotImplementedException();
        }

        protected internal void Init()
        {
            this.GotResult = false;
            this.Successful = false;
            this.PingId = (byte)(Environment.TickCount % 0xff);
        }

        public virtual bool StartPing(string ip)
        {
            throw new NotImplementedException();
        }
    }
}
