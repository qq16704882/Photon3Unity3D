﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ExitGames.Client.Photon
{
    internal class TPeer : PeerBase
    {
        //<summary>TCP header combined: 9 bytes</summary>
        public const int ALL_HEADER_BYTES = 9;
        //<summary>Defines if the(TCP) socket implementation needs to do "framing".</summary>
        //<remarks>The WebSocket protocol(e.g.) includes framing, so when that is used, we set DoFraming to false.</remarks>
        protected internal bool DoFraming;
        private Queue<byte[]> incomingList;
        private int lastPingResult;
        internal byte[] messageHeader;
        //<summary>TCP "Message" header: 2 bytes</summary>
        internal const int MSG_HEADER_BYTES = 2;
        internal List<byte[]> outgoingStream;
        private byte[] pingRequest;
        //<summary>TCP "Package" header: 7 bytes</summary>
        internal const int TCP_HEADER_BYTES = 7;
        internal static readonly byte[] tcpFramedMessageHead = new byte[] { 0xfb, 0, 0, 0, 0, 0, 0, 0xf3, 2 };
        internal static readonly byte[] tcpMsgHead = new byte[] { 0xf3, 2 };

        internal TPeer()
        {
            this.incomingList = new Queue<byte[]>(0x20);
            byte[] buffer = new byte[5];
            buffer[0] = 240;
            this.pingRequest = buffer;
            this.DoFraming = true;
            PeerBase.peerCount = (short)(PeerBase.peerCount + 1);
            base.InitOnce();
            base.TrafficPackageHeaderSize = 0;
        }

        internal TPeer(IPhotonPeerListener listener) : this()
        {
            base.Listener = listener;
        }

        internal override bool Connect(string serverAddress, string appID)
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected)
            {
                base.Listener.DebugReturn(DebugLevel.WARNING, "Connect() can't be called if peer is not Disconnected. Not connecting.");
                return false;
            }
            if (base.debugOut >= DebugLevel.ALL)
            {
                base.Listener.DebugReturn(DebugLevel.ALL, "Connect()");
            }
            base.ServerAddress = serverAddress;
            this.InitPeerBase();
            this.outgoingStream = new List<byte[]>();
            if (appID == null)
            {
                appID = "LoadBalancing";
            }
            for (int i = 0; i < 0x20; i++)
            {
                base.INIT_BYTES[i + 9] = (i < appID.Length) ? ((byte)appID[i]) : ((byte)0);
            }
            if (base.SocketImplementation != null)
            {
                base.rt = (IPhotonSocket)Activator.CreateInstance(base.SocketImplementation, new object[] { this });
            }
            else
            {
                base.rt = new SocketTcp(this);
            }
            if (base.rt == null)
            {
                base.Listener.DebugReturn(DebugLevel.ERROR, "Connect() failed, because SocketImplementation or socket was null. Set PhotonPeer.SocketImplementation before Connect(). SocketImplementation: " + base.SocketImplementation);
                return false;
            }
            this.messageHeader = this.DoFraming ? tcpFramedMessageHead : tcpMsgHead;
            if (base.rt.Connect())
            {
                base.peerConnectionState = PeerBase.ConnectionStateValue.Connecting;
                this.EnqueueInit();
                this.SendOutgoingCommands();
                return true;
            }
            base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnected;
            return false;
        }

        internal override void Disconnect()
        {
            if ((base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected) && (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnecting))
            {
                if (base.debugOut >= DebugLevel.ALL)
                {
                    base.Listener.DebugReturn(DebugLevel.ALL, "TPeer.Disconnect()");
                }
                this.StopConnection();
            }
        }
        //<summary>
        //Checks the incoming queue and Dispatches received data if possible.Returns if a Dispatch happened or
        //not, which shows if more Dispatches might be needed.
        //</summary>
        internal override bool DispatchIncomingCommands()
        {
            while (true)
            {
                PeerBase.MyAction action;
                lock (base.ActionQueue)
                {
                    if (base.ActionQueue.Count <= 0)
                    {
                        byte[] buffer;
                        lock (this.incomingList)
                        {
                            if (this.incomingList.Count <= 0)
                            {
                                return false;
                            }
                            buffer = this.incomingList.Dequeue();
                        }
                        base.ByteCountCurrentDispatch = buffer.Length + 3;
                        return this.DeserializeMessageAndCallback(buffer);
                    }
                    action = base.ActionQueue.Dequeue();
                }
                action();
            }
        }

        private void EnqueueInit()
        {
            if (this.DoFraming)
            {
                MemoryStream output = new MemoryStream(0);
                BinaryWriter writer = new BinaryWriter(output);
                //byte[] buffer3 = new byte[7];
                //buffer3[0] = 0xfb;
                //buffer3[6] = 1;
                //byte[] target = buffer3;
                byte[] target = new byte[] { 0xfb, 0, 0, 0, 0, 0, 1 };
                int targetOffset = 1;
                Protocol.Serialize((int)(base.INIT_BYTES.Length + target.Length), target, ref targetOffset);
                writer.Write(target);
                writer.Write(base.INIT_BYTES);
                byte[] opMessage = output.ToArray();
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.TotalPacketCount++;
                    base.TrafficStatsOutgoing.TotalCommandsInPackets++;
                    base.TrafficStatsOutgoing.CountControlCommand(opMessage.Length);
                }
                this.EnqueueMessageAsPayload(true, opMessage, 0);
            }
        }
        //<summary>enqueues serialized operations to be sent as tcp stream / package</summary>
        internal bool EnqueueMessageAsPayload(bool sendReliable, byte[] opMessage, byte channelId)
        {
            if (opMessage == null)
            {
                return false;
            }
            if (this.DoFraming)
            {
                opMessage[5] = channelId;
                opMessage[6] = sendReliable ? ((byte)1) : ((byte)0);
            }
            lock (this.outgoingStream)
            {
                this.outgoingStream.Add(opMessage);
                base.outgoingCommandsInStream++;
                if ((base.outgoingCommandsInStream % base.warningSize) == 0)
                {
                    base.Listener.OnStatusChanged(StatusCode.QueueOutgoingReliableWarning);
                }
            }
            int length = opMessage.Length;
            base.ByteCountLastOperation = length;
            if (base.TrafficStatsEnabled)
            {
                if (sendReliable)
                {
                    base.TrafficStatsOutgoing.CountReliableOpCommand(length);
                }
                else
                {
                    base.TrafficStatsOutgoing.CountUnreliableOpCommand(length);
                }
                base.TrafficStatsGameLevel.CountOperation(length);
            }
            return true;
        }

        internal override bool EnqueueOperation(Dictionary<byte, object> parameters, byte opCode, bool sendReliable, byte channelId, bool encrypt, PeerBase.EgMessageType messageType)
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Connected)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Cannot send op: ", opCode, "! Not connected. PeerState: ", base.peerConnectionState }));
                }
                base.Listener.OnStatusChanged(StatusCode.SendError);
                return false;
            }
            if (channelId >= base.ChannelCount)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Cannot send op: Selected channel (", channelId, ")>= channelCount (", base.ChannelCount, ")." }));
                }
                base.Listener.OnStatusChanged(StatusCode.SendError);
                return false;
            }
            byte[] opMessage = this.SerializeOperationToMessage(opCode, parameters, messageType, encrypt);
            return this.EnqueueMessageAsPayload(sendReliable, opMessage, channelId);
        }

        internal override void FetchServerTimestamp()
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Connected)
            {
                if (base.debugOut >= DebugLevel.INFO)
                {
                    base.Listener.DebugReturn(DebugLevel.INFO, "FetchServerTimestamp() was skipped, as the client is not connected. Current ConnectionState: " + base.peerConnectionState);
                }
                base.Listener.OnStatusChanged(StatusCode.SendError);
            }
            else
            {
                this.SendPing();
                base.serverTimeOffsetIsAvailable = false;
            }
        }

        internal override void InitPeerBase()
        {
            base.InitPeerBase();
            this.incomingList = new Queue<byte[]>(0x20);
        }

        private void ReadPingResult(byte[] inbuff)
        {
            int num = 0;
            int num2 = 0;
            int offset = 1;
            Protocol.Deserialize(out num, inbuff, ref offset);
            Protocol.Deserialize(out num2, inbuff, ref offset);
            base.lastRoundTripTime = SupportClass.GetTickCount() - num2;
            if (!base.serverTimeOffsetIsAvailable)
            {
                base.roundTripTime = base.lastRoundTripTime;
            }
            base.UpdateRoundTripTimeAndVariance(base.lastRoundTripTime);
            if (!base.serverTimeOffsetIsAvailable)
            {
                base.serverTimeOffset = (num + (base.lastRoundTripTime >> 1)) - SupportClass.GetTickCount();
                base.serverTimeOffsetIsAvailable = true;
            }
        }

        protected internal void ReadPingResult(OperationResponse operationResponse)
        {
            int num = (int)operationResponse.Parameters[2];
            int num2 = (int)operationResponse.Parameters[1];
            base.lastRoundTripTime = SupportClass.GetTickCount() - num2;
            if (!base.serverTimeOffsetIsAvailable)
            {
                base.roundTripTime = base.lastRoundTripTime;
            }
            base.UpdateRoundTripTimeAndVariance(base.lastRoundTripTime);
            if (!base.serverTimeOffsetIsAvailable)
            {
                base.serverTimeOffset = (num + (base.lastRoundTripTime >> 1)) - SupportClass.GetTickCount();
                base.serverTimeOffsetIsAvailable = true;
            }
        }
        //<summary>reads incoming tcp-packages to create and queue incoming commands*</summary>
        internal override void ReceiveIncomingCommands(byte[] inbuff, int dataLength)
        {
            if (inbuff == null)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.EnqueueDebugReturn(DebugLevel.ERROR, "checkAndQueueIncomingCommands() inBuff: null");
                }
            }
            else
            {
                base.timestampOfLastReceive = SupportClass.GetTickCount();
                base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                base.timeLastSendOutgoing = base.timeInt;
                base.bytesIn += inbuff.Length + 7;
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsIncoming.TotalPacketCount++;
                    base.TrafficStatsIncoming.TotalCommandsInPackets++;
                }
                if (inbuff[0] == 0xf3 || inbuff[0] == 0xf4)
                {
                    lock (this.incomingList)
                    {
                        this.incomingList.Enqueue(inbuff);
                        if ((this.incomingList.Count % base.warningSize) == 0)
                        {
                            base.EnqueueStatusCallback(StatusCode.QueueIncomingReliableWarning);
                        }
                    }
                }
                else if (inbuff[0] == 240)
                {
                    base.TrafficStatsIncoming.CountControlCommand(inbuff.Length);
                    this.ReadPingResult(inbuff);
                }
                else if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.EnqueueDebugReturn(DebugLevel.ERROR, "receiveIncomingCommands() MagicNumber should be 0xF0, 0xF3 or 0xF4. Is: " + inbuff[0]);
                }
            }
        }
        //<summary>Sends a ping in intervals to keep connection alive(server will timeout connection if nothing is sent).</summary>
        //<returns>Always false in this case (local queues are ignored. true would be: "call again to send remaining data").</returns>
        internal override bool SendAcksOnly()
        {
            if (base.rt != null && base.rt.Connected)
            {
                base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                base.timeLastSendOutgoing = base.timeInt;
                if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected && (SupportClass.GetTickCount() - this.lastPingResult > base.timePingInterval))
                {
                    this.SendPing();
                }
            }
            return false;
        }

        internal void SendData(byte[] data)
        {
            PeerBase.MyAction sendAction = null;
            try
            {
                base.bytesOut += data.Length;
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.TotalPacketCount++;
                    base.TrafficStatsOutgoing.TotalCommandsInPackets += base.outgoingCommandsInStream;
                }
                if (base.NetworkSimulationSettings.IsSimulationEnabled)
                {
                    if (sendAction == null)
                    {
                        sendAction = (PeerBase.MyAction)(() => this.rt.Send(data, data.Length));
                    }
                    base.SendNetworkSimulated(sendAction);
                }
                else
                {
                    base.rt.Send(data, data.Length);
                }
            }
            catch (Exception exception)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, exception.ToString());
                }
                SupportClass.WriteStackTrace(exception);
            }
        }
        //<summary>
        //gathers commands from all(out)queues until udp-packet is full and sends it!
        //</summary>
        internal override bool SendOutgoingCommands()
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected)
            {
                if (!base.rt.Connected)
                {
                    return false;
                }
                base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                base.timeLastSendOutgoing = base.timeInt;
                if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected && (SupportClass.GetTickCount() - this.lastPingResult > base.timePingInterval))
                {
                    this.SendPing();
                }
                lock (this.outgoingStream)
                {
                    foreach (byte[] buffer in this.outgoingStream)
                    {
                        this.SendData(buffer);
                    }
                    this.outgoingStream.Clear();
                    base.outgoingCommandsInStream = 0;
                }
            }
            return false;
        }
        //<summary>Sends a ping and modifies this.lastPingResult to avoid another ping for a while.</summary>
        internal void SendPing()
        {
            this.lastPingResult = SupportClass.GetTickCount();
            if (!this.DoFraming)
            {
                int tickCount = SupportClass.GetTickCount();
                Dictionary<byte, object> parameters = new Dictionary<byte, object> {
                {
                    1,
                    tickCount
                }
            };
                this.EnqueueOperation(parameters, PhotonCodes.Ping, true, 0, false, PeerBase.EgMessageType.InternalOperationRequest);
            }
            else
            {
                int targetOffset = 1;
                Protocol.Serialize(SupportClass.GetTickCount(), this.pingRequest, ref targetOffset);
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.CountControlCommand(this.pingRequest.Length);
                }
                this.SendData(this.pingRequest);
            }
        }
        //<summary> Returns the UDP Payload starting with Magic Number for binary protocol </summary>
        internal override byte[] SerializeOperationToMessage(byte opc, Dictionary<byte, object> parameters, PeerBase.EgMessageType messageType, bool encrypt)
        {
            byte[] buffer;
            lock (base.SerializeMemStream)
            {
                base.SerializeMemStream.Position = 0L;
                base.SerializeMemStream.SetLength(0L);
                if (!encrypt)
                {
                    base.SerializeMemStream.Write(this.messageHeader, 0, this.messageHeader.Length);
                }
                Protocol.SerializeOperationRequest(base.SerializeMemStream, opc, parameters, false);
                if (encrypt)
                {
                    byte[] data = base.SerializeMemStream.ToArray();
                    data = base.CryptoProvider.Encrypt(data);
                    base.SerializeMemStream.Position = 0L;
                    base.SerializeMemStream.SetLength(0L);
                    base.SerializeMemStream.Write(this.messageHeader, 0, this.messageHeader.Length);
                    base.SerializeMemStream.Write(data, 0, data.Length);
                }
                buffer = base.SerializeMemStream.ToArray();
            }
            if (messageType != PeerBase.EgMessageType.Operation)
            {
                buffer[this.messageHeader.Length - 1] = (byte)messageType;
            }
            if (encrypt)
            {
                buffer[this.messageHeader.Length - 1] = (byte)(buffer[this.messageHeader.Length - 1] | 0x80);
            }
            if (this.DoFraming)
            {
                int targetOffset = 1;
                Protocol.Serialize(buffer.Length, buffer, ref targetOffset);
            }
            return buffer;
        }

        internal override void StopConnection()
        {
            base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnecting;
            if (base.rt != null)
            {
                base.rt.Disconnect();
            }
            lock (this.incomingList)
            {
                this.incomingList.Clear();
            }
            base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnected;
            base.Listener.OnStatusChanged(StatusCode.Disconnect);
        }

        internal override int QueuedIncomingCommandsCount =>
            this.incomingList.Count;

        internal override int QueuedOutgoingCommandsCount =>
            base.outgoingCommandsInStream;
    }
}