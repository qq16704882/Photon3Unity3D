﻿using System.Collections;
using System.Collections.Generic;

namespace ExitGames.Client.Photon
{
    //<summary>
    //This is a substitute for the Hashtable class, missing in: Win8RT and Windows Phone.It uses a Dictionary&lt; object,object&gt; as base.
    //</summary>
    //<remarks>
    //Please be aware that this class might act differently than the Hashtable equivalent.
    //As far as Photon is concerned, the substitution is sufficiently precise.
    //</remarks>
    public class Hashtable : Dictionary<object, object>
    {
        private DictionaryEntryEnumerator enumerator;

        public Hashtable()
        {
        }

        public Hashtable(int x) : base(x)
        {
        }
        //<summary>
        //Creates a shallow copy of the Hashtable.
        //</summary>
        //<remarks>
        //A shallow copy of a collection copies only the elements of the collection, whether they are
        //reference types or value types, but it does not copy the objects that the references refer
        //to.The references in the new collection point to the same objects that the references in
        //the original collection point to.
        //</remarks>
        //<returns>Shallow copy of the Hashtable.</returns>
        public object Clone() =>
            new Dictionary<object, object>(this);

        public new IEnumerator<DictionaryEntry> GetEnumerator() =>
            new DictionaryEntryEnumerator(base.GetEnumerator());

        public override string ToString()
        {
            List<string> list = new List<string>();
            foreach (object obj2 in base.Keys)
            {
                if ((obj2 == null) || (this[obj2] == null))
                {
                    list.Add(obj2 + "=" + this[obj2]);
                }
                else
                {
                    list.Add(string.Concat(new object[] { "(", obj2.GetType(), ")", obj2, "=(", this[obj2].GetType(), ")", this[obj2] }));
                }
            }
            return string.Join(", ", list.ToArray());
        }

        public new object this[object key]
        {
            get
            {
                object obj2 = null;
                base.TryGetValue(key, out obj2);
                return obj2;
            }
            set
            {
                base[key] = value;
            }
        }
    }
}
