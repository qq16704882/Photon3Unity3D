﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExitGames.Client.Photon
{
    internal class EnetPeer : PeerBase
    {
        internal int challenge;
        private EnetChannel[] channelArray;
        //<summary>Will contain channel 0xFF and any other.</summary>
        private Dictionary<byte, EnetChannel> channels;
        private Queue<int> commandsToRemove;
        private const int CRC_LENGTH = 4;
        private byte[] initData;
        internal static readonly byte[] messageHeader = udpHeader0xF3;
        //<summary>One list for all channels keeps acknowledgements.</summary>
        private Queue<NCommand> outgoingAcknowledgementsList;
        internal int reliableCommandsRepeated;
        internal int reliableCommandsSent;
        //<summary>One list for all channels keeps sent commands (for re-sending).</summary>
        private List<NCommand> sentReliableCommands;
        internal int serverSentTime;
        private byte[] udpBuffer;
        private int udpBufferIndex;
        private byte udpCommandCount;
        internal static readonly byte[] udpHeader0xF3 = new byte[] { 0xf3, 2 };
        internal readonly int windowSize;

        internal EnetPeer()
        {
            this.channels = new Dictionary<byte, EnetChannel>();
            this.sentReliableCommands = new List<NCommand>();
            this.outgoingAcknowledgementsList = new Queue<NCommand>();
            this.windowSize = 0x80;//128
            this.initData = null;
            this.channelArray = new EnetChannel[0];
            this.commandsToRemove = new Queue<int>();
            PeerBase.peerCount = (short)(PeerBase.peerCount + 1);
            base.InitOnce();
            base.TrafficPackageHeaderSize = 12;
        }

        internal EnetPeer(IPhotonPeerListener listener) : this()
        {
            base.Listener = listener;
        }
        //<summary>
        //Checks if any channel has a outgoing reliable command.
        //</summary>
        //<returns>True if any channel has a outgoing reliable command.False otherwise.</returns>
        private bool AreReliableCommandsInTransit()
        {
            lock (this.channels)
            {
                foreach (EnetChannel channel in this.channels.Values)
                {
                    if (channel.outgoingReliableCommandsList.Count > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        internal string CommandListToString(NCommand[] list)
        {
            if (base.debugOut < DebugLevel.ALL)
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < list.Length; i++)
            {
                builder.Append(i + "=");
                builder.Append(list[i]);
                builder.Append(" # ");
            }
            return builder.ToString();
        }

        internal override bool Connect(string ipport, string appID)
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected)
            {
                base.Listener.DebugReturn(DebugLevel.WARNING, "Connect() can't be called if peer is not Disconnected. Not connecting. peerConnectionState: " + base.peerConnectionState);
                return false;
            }
            if (base.debugOut >= DebugLevel.ALL)
            {
                base.Listener.DebugReturn(DebugLevel.ALL, "Connect()");
            }
            base.ServerAddress = ipport;
            this.InitPeerBase();
            if (appID == null)
            {
                appID = "LoadBalancing";
            }
            for (int i = 0; i < NCommand.CmdSizeFragmentHeader; i++)
            {
                base.INIT_BYTES[i + 9] = (i < appID.Length) ? ((byte)appID[i]) : ((byte)0);
            }
            this.initData = base.INIT_BYTES;
            base.rt = new SocketUdp(this);
            if (base.rt == null)
            {
                base.Listener.DebugReturn(DebugLevel.ERROR, "Connect() failed, because SocketImplementation or socket was null. Set PhotonPeer.SocketImplementation before Connect().");
                return false;
            }
            if (base.rt.Connect())
            {
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.ControlCommandBytes += NCommand.CmdSizeConnect;
                    base.TrafficStatsOutgoing.ControlCommandCount++;
                }
                base.peerConnectionState = PeerBase.ConnectionStateValue.Connecting;
                this.QueueOutgoingReliableCommand(new NCommand(this, NCommand.CT_CONNECT, null, 0xff));
                return true;
            }
            return false;
        }
        //<summary>reliable-udp-level function to send some byte[] to the server via un/reliable command</summary>
        //<remarks>only called when a custom operation should be send</remarks>
        //<param name = "commandType" > (enet)command type</param>
        //<param name = "payload" > data to carry (operation)</param>
        //<param name = "channelNumber" > channel in which to send</param>
        //<returns>the invocation ID for this operation (the payload)</returns>
        internal bool CreateAndEnqueueCommand(byte commandType, byte[] payload, byte channelNumber)
        {
            NCommand command;
            if (payload == null)
            {
                return false;
            }
            EnetChannel channel = this.channels[channelNumber];
            base.ByteCountLastOperation = 0;
            int count = (base.mtu - NCommand.HEADER_UDP_PACK_LENGTH) - NCommand.CmdSizeMaxHeader;
            if (payload.Length > count)
            {
                int fragmentCount = (payload.Length + count - 1) / count;
                int startSequenceNumber = channel.outgoingReliableSequenceNumber + 1;
                int fragmentNumber = 0;
                for (int i = 0; i < payload.Length; i += count)
                {
                    if ((payload.Length - i) < count)
                    {
                        count = payload.Length - i;
                    }
                    byte[] dst = new byte[count];
                    Buffer.BlockCopy(payload, i, dst, 0, count);
                    command = new NCommand(this, NCommand.CT_SENDFRAGMENT, dst, channel.ChannelNumber)
                    {
                        fragmentNumber = fragmentNumber,
                        startSequenceNumber = startSequenceNumber,
                        fragmentCount = fragmentCount,
                        totalLength = payload.Length,
                        fragmentOffset = i
                    };
                    this.QueueOutgoingReliableCommand(command);
                    base.ByteCountLastOperation += command.Size;
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsOutgoing.CountFragmentOpCommand(command.Size);
                        base.TrafficStatsGameLevel.CountOperation(command.Size);
                    }
                    fragmentNumber++;
                }
            }
            else
            {
                command = new NCommand(this, commandType, payload, channel.ChannelNumber);
                if (command.commandFlags == NCommand.FV_RELIABLE)
                {
                    this.QueueOutgoingReliableCommand(command);
                    base.ByteCountLastOperation = command.Size;
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsOutgoing.CountReliableOpCommand(command.Size);
                        base.TrafficStatsGameLevel.CountOperation(command.Size);
                    }
                }
                else
                {
                    this.QueueOutgoingUnreliableCommand(command);
                    base.ByteCountLastOperation = command.Size;
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsOutgoing.CountUnreliableOpCommand(command.Size);
                        base.TrafficStatsGameLevel.CountOperation(command.Size);
                    }
                }
            }
            return true;
        }

        internal override void Disconnect()
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected && base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnecting)
            {
                if (this.outgoingAcknowledgementsList != null)
                {
                    lock (this.outgoingAcknowledgementsList)
                    {
                        this.outgoingAcknowledgementsList.Clear();
                    }
                }
                if (this.sentReliableCommands != null)
                {
                    lock (this.sentReliableCommands)
                    {
                        this.sentReliableCommands.Clear();
                    }
                }
                lock (this.channels)
                {
                    foreach (EnetChannel channel in this.channels.Values)
                    {
                        channel.clearAll();
                    }
                }
                bool isSimulationEnabled = base.NetworkSimulationSettings.IsSimulationEnabled;
                base.NetworkSimulationSettings.IsSimulationEnabled = false;
                NCommand command = new NCommand(this, NCommand.CT_DISCONNECT, null, 0xff);
                this.QueueOutgoingReliableCommand(command);
                this.SendOutgoingCommands();
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.CountControlCommand(command.Size);
                }
                base.rt.Disconnect();
                base.NetworkSimulationSettings.IsSimulationEnabled = isSimulationEnabled;
                base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnected;
                base.Listener.OnStatusChanged(StatusCode.Disconnect);
            }
        }
        //<summary>
        //Checks the incoming queue and Dispatches received data if possible.
        //</summary>
        //<returns>If a Dispatch happened or not, which shows if more Dispatches might be needed.</returns>
        internal override bool DispatchIncomingCommands()
        {
            NCommand command;
            while (true)
            {
                PeerBase.MyAction action;
                lock (base.ActionQueue)
                {
                    if (base.ActionQueue.Count <= 0)
                    {
                        command = null;
                        lock (this.channels)
                        {
                            for (int i = 0; i < this.channelArray.Length; i++)
                            {
                                EnetChannel channel = this.channelArray[i];
                                if (channel.incomingUnreliableCommandsList.Count > 0)
                                {
                                    int num2 = 0x7fffffff;
                                    foreach (int num3 in channel.incomingUnreliableCommandsList.Keys)
                                    {
                                        NCommand command2 = channel.incomingUnreliableCommandsList[num3];
                                        if (num3 < channel.incomingUnreliableSequenceNumber || command2.reliableSequenceNumber < channel.incomingReliableSequenceNumber)
                                        {
                                            this.commandsToRemove.Enqueue(num3);
                                        }
                                        else if (base.limitOfUnreliableCommands > 0 && channel.incomingUnreliableCommandsList.Count > base.limitOfUnreliableCommands)
                                        {
                                            this.commandsToRemove.Enqueue(num3);
                                        }
                                        else if ((num3 < num2) && (command2.reliableSequenceNumber <= channel.incomingReliableSequenceNumber))
                                        {
                                            num2 = num3;
                                        }
                                    }
                                    while (this.commandsToRemove.Count > 0)
                                    {
                                        channel.incomingUnreliableCommandsList.Remove(this.commandsToRemove.Dequeue());
                                    }
                                    if (num2 < 0x7fffffff)
                                    {
                                        command = channel.incomingUnreliableCommandsList[num2];
                                    }
                                    if (command != null)
                                    {
                                        channel.incomingUnreliableCommandsList.Remove(command.unreliableSequenceNumber);
                                        channel.incomingUnreliableSequenceNumber = command.unreliableSequenceNumber;
                                        break;
                                    }
                                }
                                if (command == null && channel.incomingReliableCommandsList.Count > 0)
                                {
                                    channel.incomingReliableCommandsList.TryGetValue(channel.incomingReliableSequenceNumber + 1, out command);
                                    if (command != null)
                                    {
                                        if (command.commandType != NCommand.CT_SENDFRAGMENT)
                                        {
                                            channel.incomingReliableSequenceNumber = command.reliableSequenceNumber;
                                            channel.incomingReliableCommandsList.Remove(command.reliableSequenceNumber);
                                        }
                                        else if (command.fragmentsRemaining > 0)
                                        {
                                            command = null;
                                        }
                                        else
                                        {
                                            byte[] dst = new byte[command.totalLength];
                                            for (int j = command.startSequenceNumber; j < (command.startSequenceNumber + command.fragmentCount); j++)
                                            {
                                                if (!channel.ContainsReliableSequenceNumber(j))
                                                {
                                                    throw new Exception("command.fragmentsRemaining was 0, but not all fragments are found to be combined!");
                                                }
                                                NCommand command3 = channel.FetchReliableSequenceNumber(j);
                                                Buffer.BlockCopy(command3.Payload, 0, dst, command3.fragmentOffset, command3.Payload.Length);
                                                channel.incomingReliableCommandsList.Remove(command3.reliableSequenceNumber);
                                            }
                                            if (base.debugOut >= DebugLevel.ALL)
                                            {
                                                base.Listener.DebugReturn(DebugLevel.ALL, "assembled fragmented payload from " + command.fragmentCount + " parts. Dispatching now.");
                                            }
                                            command.Payload = dst;
                                            command.Size = (12 * command.fragmentCount) + command.totalLength;
                                            channel.incomingReliableSequenceNumber = (command.reliableSequenceNumber + command.fragmentCount) - 1;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    action = base.ActionQueue.Dequeue();
                }
                action();
            }
            if (command != null && command.Payload != null)
            {
                base.ByteCountCurrentDispatch = command.Size;
                base.CommandInCurrentDispatch = command;
                if (this.DeserializeMessageAndCallback(command.Payload))
                {
                    base.CommandInCurrentDispatch = null;
                    return true;
                }
                base.CommandInCurrentDispatch = null;
            }
            return false;
        }
        //<summary>
        //Checks connected state and channel before operation is serialized and enqueued for sending.
        //</summary>
        //<param name = "parameters" > operation parameters</param>
        //<param name = "opCode" > code of operation</param>
        //<param name = "sendReliable" > send as reliable command</param>
        //    <param name = "channelId" > channel(sequence) for command</param>
        //    <param name = "encrypt" > encrypt or not</param>
        //<param name = "messageType" > usually EgMessageType.Operation</param>
        //    <returns>if operation could be enqueued</returns>
        internal override bool EnqueueOperation(Dictionary<byte, object> parameters, byte opCode, bool sendReliable, byte channelId, bool encrypt, PeerBase.EgMessageType messageType)
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Connected)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Cannot send op: ", opCode, " Not connected. PeerState: ", base.peerConnectionState }));
                }
                base.Listener.OnStatusChanged(StatusCode.SendError);
                return false;
            }
            if (channelId >= base.ChannelCount)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Cannot send op: Selected channel (", channelId, ")>= channelCount (", base.ChannelCount, ")." }));
                }
                base.Listener.OnStatusChanged(StatusCode.SendError);
                return false;
            }
            byte[] payload = this.SerializeOperationToMessage(opCode, parameters, messageType, encrypt);
            return this.CreateAndEnqueueCommand(sendReliable ? NCommand.CT_SENDRELIABLE : NCommand.CT_SENDUNRELIABLE, payload, channelId);
        }

        internal bool ExecuteCommand(NCommand command)
        {
            PeerBase.MyAction action = null;
            bool flag = true;
            switch (command.commandType)
            {
                case NCommand.CT_ACK:
                    {
                        if (base.TrafficStatsEnabled)
                        {
                            base.TrafficStatsIncoming.CountControlCommand(command.Size);
                        }
                        base.timeLastAckReceive = base.timeInt;
                        base.lastRoundTripTime = base.timeInt - command.ackReceivedSentTime;
                        NCommand command2 = this.RemoveSentReliableCommand(command.ackReceivedReliableSequenceNumber, command.commandChannelID);
                        if (base.CommandLog != null)
                        {
                            base.CommandLog.Enqueue(new CmdLogReceivedAck(command, base.timeInt, base.roundTripTime, base.roundTripTimeVariance));
                            base.CommandLogResize();
                        }
                        if (command2 != null)
                        {
                            if (command2.commandType != NCommand.CT_EG_SERVERTIME)
                            {
                                base.UpdateRoundTripTimeAndVariance(base.lastRoundTripTime);
                                if ((command2.commandType == NCommand.CT_DISCONNECT) && (base.peerConnectionState == PeerBase.ConnectionStateValue.Disconnecting))
                                {
                                    if (base.debugOut >= DebugLevel.INFO)
                                    {
                                        base.EnqueueDebugReturn(DebugLevel.INFO, "Received disconnect ACK by server");
                                    }
                                    if (action == null)
                                    {
                                        action = delegate
                                        {
                                            base.rt.Disconnect();
                                        };
                                    }
                                    base.EnqueueActionForDispatch(action);
                                    return flag;
                                }
                                if (command2.commandType == NCommand.CT_CONNECT)
                                {
                                    base.roundTripTime = base.lastRoundTripTime;
                                }
                                return flag;
                            }
                            if (base.lastRoundTripTime <= base.roundTripTime)
                            {
                                base.serverTimeOffset = (this.serverSentTime + (base.lastRoundTripTime >> 1)) - SupportClass.GetTickCount();
                                base.serverTimeOffsetIsAvailable = true;
                                return flag;
                            }
                            this.FetchServerTimestamp();
                        }
                        return flag;
                    }
                case NCommand.CT_CONNECT:
                case NCommand.CT_PING:
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsIncoming.CountControlCommand(command.Size);
                    }
                    return flag;

                case NCommand.CT_VERIFYCONNECT:
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsIncoming.CountControlCommand(command.Size);
                    }
                    if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connecting)
                    {
                        command = new NCommand(this, NCommand.CT_SENDRELIABLE, this.initData, 0);
                        this.QueueOutgoingReliableCommand(command);
                        if (base.TrafficStatsEnabled)
                        {
                            base.TrafficStatsOutgoing.CountControlCommand(command.Size);
                        }
                        base.peerConnectionState = PeerBase.ConnectionStateValue.Connected;
                    }
                    return flag;

                case NCommand.CT_DISCONNECT:
                    {
                        if (base.TrafficStatsEnabled)
                        {
                            base.TrafficStatsIncoming.CountControlCommand(command.Size);
                        }
                        StatusCode disconnectByServer = StatusCode.DisconnectByServer;
                        if (command.reservedByte == 1)
                        {
                            disconnectByServer = StatusCode.DisconnectByServerLogic;
                        }
                        else if (command.reservedByte == 3)
                        {
                            disconnectByServer = StatusCode.DisconnectByServerUserLimit;
                        }
                        if (base.debugOut >= DebugLevel.INFO)
                        {
                            base.Listener.DebugReturn(DebugLevel.INFO, string.Concat(new object[] { "Server ", base.ServerAddress, " sent disconnect. PeerId: ", (ushort)base.peerID, " RTT/Variance:", base.roundTripTime, "/", base.roundTripTimeVariance, " reason byte: ", command.reservedByte }));
                        }
                        base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnecting;
                        base.Listener.OnStatusChanged(disconnectByServer);
                        base.rt.Disconnect();
                        base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnected;
                        base.Listener.OnStatusChanged(StatusCode.Disconnect);
                        return flag;
                    }
                case NCommand.CT_SENDRELIABLE:
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsIncoming.CountReliableOpCommand(command.Size);
                    }
                    if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected)
                    {
                        flag = this.QueueIncomingCommand(command);
                    }
                    return flag;

                case NCommand.CT_SENDUNRELIABLE:
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsIncoming.CountUnreliableOpCommand(command.Size);
                    }
                    if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected)
                    {
                        flag = this.QueueIncomingCommand(command);
                    }
                    return flag;

                case NCommand.CT_SENDFRAGMENT:
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsIncoming.CountFragmentOpCommand(command.Size);
                    }
                    if (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected)
                    {
                        if (((command.fragmentNumber > command.fragmentCount) || (command.fragmentOffset >= command.totalLength)) || ((command.fragmentOffset + command.Payload.Length) > command.totalLength))
                        {
                            if (base.debugOut >= DebugLevel.ERROR)
                            {
                                base.Listener.DebugReturn(DebugLevel.ERROR, "Received fragment has bad size: " + command);
                            }
                            return flag;
                        }
                        flag = this.QueueIncomingCommand(command);
                        if (!flag)
                        {
                            return flag;
                        }
                        EnetChannel channel = this.channels[command.commandChannelID];
                        if (command.reliableSequenceNumber != command.startSequenceNumber)
                        {
                            if (channel.ContainsReliableSequenceNumber(command.startSequenceNumber))
                            {
                                NCommand command1 = channel.FetchReliableSequenceNumber(command.startSequenceNumber);
                                command1.fragmentsRemaining--;
                            }
                            return flag;
                        }
                        command.fragmentsRemaining--;
                        int num = command.startSequenceNumber + 1;
                        while ((command.fragmentsRemaining > 0) && (num < (command.startSequenceNumber + command.fragmentCount)))
                        {
                            if (channel.ContainsReliableSequenceNumber(num++))
                            {
                                command.fragmentsRemaining--;
                            }
                        }
                    }
                    return flag;
            }
            return flag;
        }

        internal override void FetchServerTimestamp()
        {
            if (base.peerConnectionState != PeerBase.ConnectionStateValue.Connected)
            {
                if (base.debugOut >= DebugLevel.INFO)
                {
                    base.EnqueueDebugReturn(DebugLevel.INFO, "FetchServerTimestamp() was skipped, as the client is not connected. Current ConnectionState: " + base.peerConnectionState);
                }
            }
            else
            {
                this.CreateAndEnqueueCommand(NCommand.CT_EG_SERVERTIME, new byte[0], 0xff);
            }
        }

        internal override void InitPeerBase()
        {
            Dictionary<byte, EnetChannel> dictionary;
            base.InitPeerBase();
            base.peerID = -1;
            this.challenge = SupportClass.ThreadSafeRandom.Next();
            this.udpBuffer = new byte[base.mtu];
            this.reliableCommandsSent = 0;
            this.reliableCommandsRepeated = 0;
            lock ((dictionary = this.channels))
            {
                this.channels = new Dictionary<byte, EnetChannel>();
            }
            lock ((dictionary = this.channels))
            {
                this.channels[0xff] = new EnetChannel(0xff, base.commandBufferSize);
                for (byte i = 0; i < base.ChannelCount; i = (byte)(i + 1))
                {
                    this.channels[i] = new EnetChannel(i, base.commandBufferSize);
                }
                this.channelArray = new EnetChannel[base.ChannelCount + 1];
                int num2 = 0;
                foreach (EnetChannel channel in this.channels.Values)
                {
                    this.channelArray[num2++] = channel;
                }
            }
            lock (this.sentReliableCommands)
            {
                this.sentReliableCommands = new List<NCommand>(base.commandBufferSize);
            }
            lock (this.outgoingAcknowledgementsList)
            {
                this.outgoingAcknowledgementsList = new Queue<NCommand>(base.commandBufferSize);
            }
            base.CommandLogInit();
        }

        internal bool QueueIncomingCommand(NCommand command)
        {
            this.channels.TryGetValue(command.commandChannelID, out EnetChannel channel);
            if (channel == null)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, "Received command for non-existing channel: " + command.commandChannelID);
                }
                return false;
            }
            if (base.debugOut >= DebugLevel.ALL)
            {
                base.Listener.DebugReturn(DebugLevel.ALL, string.Concat(new object[] { "queueIncomingCommand() ", command, " channel seq# r/u: ", channel.incomingReliableSequenceNumber, "/", channel.incomingUnreliableSequenceNumber }));
            }
            if (command.commandFlags == NCommand.FV_RELIABLE)
            {
                if (command.reliableSequenceNumber <= channel.incomingReliableSequenceNumber)
                {
                    if (base.debugOut >= DebugLevel.INFO)
                    {
                        base.Listener.DebugReturn(DebugLevel.INFO, string.Concat(new object[] { "incoming command ", command, " is old (not saving it). Dispatched incomingReliableSequenceNumber: ", channel.incomingReliableSequenceNumber }));
                    }
                    return false;
                }
                if (channel.ContainsReliableSequenceNumber(command.reliableSequenceNumber))
                {
                    if (base.debugOut >= DebugLevel.INFO)
                    {
                        base.Listener.DebugReturn(DebugLevel.INFO, string.Concat(new object[] { "Info: command was received before! Old/New: ", channel.FetchReliableSequenceNumber(command.reliableSequenceNumber), "/", command, " inReliableSeq#: ", channel.incomingReliableSequenceNumber }));
                    }
                    return false;
                }
                if ((channel.incomingReliableCommandsList.Count >= base.warningSize) && ((channel.incomingReliableCommandsList.Count % base.warningSize) == 0))
                {
                    base.Listener.OnStatusChanged(StatusCode.QueueIncomingReliableWarning);
                }
                channel.incomingReliableCommandsList.Add(command.reliableSequenceNumber, command);
                return true;
            }
            if (command.commandFlags == NCommand.FV_UNRELIABLE)
            {
                if (command.reliableSequenceNumber < channel.incomingReliableSequenceNumber)
                {
                    if (base.debugOut >= DebugLevel.INFO)
                    {
                        base.Listener.DebugReturn(DebugLevel.INFO, "incoming reliable-seq# < Dispatched-rel-seq#. not saved.");
                    }
                    return true;
                }
                if (command.unreliableSequenceNumber <= channel.incomingUnreliableSequenceNumber)
                {
                    if (base.debugOut >= DebugLevel.INFO)
                    {
                        base.Listener.DebugReturn(DebugLevel.INFO, "incoming unreliable-seq# < Dispatched-unrel-seq#. not saved.");
                    }
                    return true;
                }
                if (channel.ContainsUnreliableSequenceNumber(command.unreliableSequenceNumber))
                {
                    if (base.debugOut >= DebugLevel.INFO)
                    {
                        base.Listener.DebugReturn(DebugLevel.INFO, string.Concat(new object[] { "command was received before! Old/New: ", channel.incomingUnreliableCommandsList[command.unreliableSequenceNumber], "/", command }));
                    }
                    return false;
                }
                if ((channel.incomingUnreliableCommandsList.Count >= base.warningSize) && ((channel.incomingUnreliableCommandsList.Count % base.warningSize) == 0))
                {
                    base.Listener.OnStatusChanged(StatusCode.QueueIncomingUnreliableWarning);
                }
                channel.incomingUnreliableCommandsList.Add(command.unreliableSequenceNumber, command);
                return true;
            }
            return false;
        }

        internal void QueueOutgoingAcknowledgement(NCommand command)
        {
            lock (this.outgoingAcknowledgementsList)
            {
                if (this.outgoingAcknowledgementsList.Count >= base.warningSize && (this.outgoingAcknowledgementsList.Count % base.warningSize) == 0)
                {
                    base.Listener.OnStatusChanged(StatusCode.QueueOutgoingAcksWarning);
                }
                this.outgoingAcknowledgementsList.Enqueue(command);
            }
        }

        internal void QueueOutgoingReliableCommand(NCommand command)
        {
            EnetChannel channel = this.channels[command.commandChannelID];
            lock (channel)
            {
                Queue<NCommand> outgoingReliableCommandsList = channel.outgoingReliableCommandsList;
                if (outgoingReliableCommandsList.Count >= base.warningSize && (outgoingReliableCommandsList.Count % base.warningSize) == 0)
                {
                    base.Listener.OnStatusChanged(StatusCode.QueueOutgoingReliableWarning);
                }
                if (command.reliableSequenceNumber == 0)
                {
                    command.reliableSequenceNumber = ++channel.outgoingReliableSequenceNumber;
                }
                outgoingReliableCommandsList.Enqueue(command);
            }
        }

        internal void QueueOutgoingUnreliableCommand(NCommand command)
        {
            Queue<NCommand> outgoingUnreliableCommandsList = this.channels[command.commandChannelID].outgoingUnreliableCommandsList;
            if (outgoingUnreliableCommandsList.Count >= base.warningSize && (outgoingUnreliableCommandsList.Count % base.warningSize) == 0)
            {
                base.Listener.OnStatusChanged(StatusCode.QueueOutgoingUnreliableWarning);
            }
            EnetChannel channel = this.channels[command.commandChannelID];
            command.reliableSequenceNumber = channel.outgoingReliableSequenceNumber;
            command.unreliableSequenceNumber = ++channel.outgoingUnreliableSequenceNumber;
            outgoingUnreliableCommandsList.Enqueue(command);
        }

        internal void QueueSentCommand(NCommand command)
        {
            command.commandSentTime = base.timeInt;
            command.commandSentCount = (byte)(command.commandSentCount + 1);
            if (command.roundTripTimeout == 0)
            {
                command.roundTripTimeout = base.roundTripTime + (4 * base.roundTripTimeVariance);
                command.timeoutTime = base.timeInt + base.DisconnectTimeout;
            }
            else if (command.commandSentCount > base.QuickResendAttempts + 1)
            {
                command.roundTripTimeout *= 2;
            }
            lock (this.sentReliableCommands)
            {
                if (this.sentReliableCommands.Count == 0)
                {
                    int num = command.commandSentTime + command.roundTripTimeout;
                    if (num < base.timeoutInt)
                    {
                        base.timeoutInt = num;
                    }
                }
                this.reliableCommandsSent++;
                this.sentReliableCommands.Add(command);
            }
            if (this.sentReliableCommands.Count >= base.warningSize && (this.sentReliableCommands.Count % base.warningSize) == 0)
            {
                base.Listener.OnStatusChanged(StatusCode.QueueSentWarning);
            }
        }
        //<summary>reads incoming udp-packages to create and queue incoming commands*</summary>
        internal override void ReceiveIncomingCommands(byte[] inBuff, int dataLength)
        {
            base.timestampOfLastReceive = SupportClass.GetTickCount();
            try
            {
                int offset = 0;
                Protocol.Deserialize(out short tempPeerID, inBuff, ref offset);
                byte tempCrcEnabled = inBuff[offset++];
                byte tempUdpCommandCount = inBuff[offset++];
                Protocol.Deserialize(out this.serverSentTime, inBuff, ref offset);
                Protocol.Deserialize(out int tempChallenge, inBuff, ref offset);
                if (tempCrcEnabled == 0xcc)
                {
                    Protocol.Deserialize(out int tempCrc, inBuff, ref offset);
                    base.bytesIn += 4L;
                    offset -= 4;
                    Protocol.Serialize(0, inBuff, ref offset);
                    uint crc = SupportClass.CalculateCrc(inBuff, dataLength);
                    if (tempCrc != crc)
                    {
                        base.packetLossByCrc++;
                        if ((base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected) && (base.debugOut >= DebugLevel.INFO))
                        {
                            base.EnqueueDebugReturn(DebugLevel.INFO, $"Ignored package due to wrong CRC. Incoming:  {(uint)tempCrc:X} Local: {crc:X}");
                        }
                        return;
                    }
                }
                base.bytesIn += 12L;
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsIncoming.TotalPacketCount++;
                    base.TrafficStatsIncoming.TotalCommandsInPackets += tempUdpCommandCount;
                }
                if (tempUdpCommandCount > base.commandBufferSize || tempUdpCommandCount <= 0)
                {
                    base.EnqueueDebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "too many/few incoming commands in package: ", tempUdpCommandCount, " > ", base.commandBufferSize }));
                }
                if (tempChallenge != this.challenge)
                {
                    base.packetLossByChallenge++;
                    if (base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnected && base.debugOut >= DebugLevel.ALL)
                    {
                        base.EnqueueDebugReturn(DebugLevel.ALL, string.Concat(new object[] { "Info: Ignoring received package due to wrong challenge. Challenge in-package!=local:", tempChallenge, "!=", this.challenge, " Commands in it: ", tempUdpCommandCount }));
                    }
                }
                else
                {
                    base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                    for (int i = 0; i < tempUdpCommandCount; i++)
                    {
                        PeerBase.MyAction action = null;
                        NCommand readCommand = new NCommand(this, inBuff, ref offset);
                        if (readCommand.commandType != 1)
                        {
                            if (action == null)
                            {
                                action = (PeerBase.MyAction)(() => this.ExecuteCommand(readCommand));
                            }
                            base.EnqueueActionForDispatch(action);
                        }
                        else
                        {
                            base.TrafficStatsIncoming.TimestampOfLastAck = SupportClass.GetTickCount();
                            this.ExecuteCommand(readCommand);
                        }
                        if ((readCommand.commandFlags & 1) > 0)
                        {
                            if (base.InReliableLog != null)
                            {
                                base.InReliableLog.Enqueue(new CmdLogReceivedReliable(readCommand, base.timeInt, base.roundTripTime, base.roundTripTimeVariance, base.timeInt - base.timeLastSendOutgoing, base.timeInt - base.timeLastSendAck));
                                base.CommandLogResize();
                            }
                            NCommand command = NCommand.CreateAck(this, readCommand, this.serverSentTime);
                            this.QueueOutgoingAcknowledgement(command);
                            if (base.TrafficStatsEnabled)
                            {
                                base.TrafficStatsIncoming.TimestampOfLastReliableCommand = SupportClass.GetTickCount();
                                base.TrafficStatsOutgoing.CountControlCommand(command.Size);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.EnqueueDebugReturn(DebugLevel.ERROR, $"Exception while reading commands from incoming data: {exception}");
                }
                SupportClass.WriteStackTrace(exception);
            }
        }
        //<summary>removes commands which are acknowledged*</summary>
        internal NCommand RemoveSentReliableCommand(int ackReceivedReliableSequenceNumber, int ackReceivedChannel)
        {
            NCommand item = null;
            lock (this.sentReliableCommands)
            {
                foreach (NCommand command in this.sentReliableCommands)
                {
                    if (((command != null) && (command.reliableSequenceNumber == ackReceivedReliableSequenceNumber)) && (command.commandChannelID == ackReceivedChannel))
                    {
                        item = command;
                        break;
                    }
                }
                if (item != null)
                {
                    this.sentReliableCommands.Remove(item);
                    if (this.sentReliableCommands.Count > 0)
                    {
                        base.timeoutInt = base.timeInt + 0x19;
                    }
                    return item;
                }
                if ((base.debugOut >= DebugLevel.ALL && base.peerConnectionState != PeerBase.ConnectionStateValue.Connected) && base.peerConnectionState != PeerBase.ConnectionStateValue.Disconnecting)
                {
                    base.EnqueueDebugReturn(DebugLevel.ALL, $"No sent command for ACK (Ch: {ackReceivedReliableSequenceNumber} Sq#: {ackReceivedChannel}). PeerState: {base.peerConnectionState}.");
                }
            }
            return item;
        }
        //<summary>
        //gathers acks until udp-packet is full and sends it!
        //</summary>
        internal override bool SendAcksOnly()
        {
            if (base.peerConnectionState == PeerBase.ConnectionStateValue.Disconnected)
            {
                return false;
            }
            if (!((base.rt != null) && base.rt.Connected))
            {
                return false;
            }
            lock (this.udpBuffer)
            {
                int num = 0;
                this.udpBufferIndex = 12;
                if (base.crcEnabled)
                {
                    this.udpBufferIndex += 4;
                }
                this.udpCommandCount = 0;
                base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                lock (this.outgoingAcknowledgementsList)
                {
                    if (this.outgoingAcknowledgementsList.Count > 0)
                    {
                        num = this.SerializeToBuffer(this.outgoingAcknowledgementsList);
                        base.timeLastSendAck = base.timeInt;
                    }
                }
                if (base.timeInt > base.timeoutInt && this.sentReliableCommands.Count > 0)
                {
                    lock (this.sentReliableCommands)
                    {
                        foreach (NCommand command in this.sentReliableCommands)
                        {
                            if ((command != null && command.roundTripTimeout != 0) && (base.timeInt - command.commandSentTime > command.roundTripTimeout))
                            {
                                command.commandSentCount = 1;
                                command.roundTripTimeout = 0;
                                command.timeoutTime = 0x7fffffff;
                                command.commandSentTime = base.timeInt;
                            }
                        }
                    }
                }
                if (this.udpCommandCount <= 0)
                {
                    return false;
                }
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.TotalPacketCount++;
                    base.TrafficStatsOutgoing.TotalCommandsInPackets += this.udpCommandCount;
                }
                this.SendData(this.udpBuffer, this.udpBufferIndex);
                return (num > 0);
            }
        }

        internal void SendData(byte[] data, int length)
        {
            try
            {
                int targetOffset = 0;
                Protocol.Serialize(base.peerID, data, ref targetOffset);
                data[2] = base.crcEnabled ? ((byte)0xcc) : ((byte)0);
                data[3] = this.udpCommandCount;
                targetOffset = 4;
                Protocol.Serialize(base.timeInt, data, ref targetOffset);
                Protocol.Serialize(this.challenge, data, ref targetOffset);
                if (base.crcEnabled)
                {
                    Protocol.Serialize(0, data, ref targetOffset);
                    uint crc = SupportClass.CalculateCrc(data, length);
                    targetOffset -= 4;
                    Protocol.Serialize((int)crc, data, ref targetOffset);
                }
                base.bytesOut += length;
                if (base.NetworkSimulationSettings.IsSimulationEnabled)
                {
                    byte[] dataCopy = new byte[length];
                    Buffer.BlockCopy(data, 0, dataCopy, 0, length);
                    base.SendNetworkSimulated((PeerBase.MyAction)(() => this.rt.Send(dataCopy, length)));
                }
                else
                {
                    base.rt.Send(data, length);
                }
            }
            catch (Exception exception)
            {
                if (base.debugOut >= DebugLevel.ERROR)
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, exception.ToString());
                }
                SupportClass.WriteStackTrace(exception);
            }
        }
        //<summary>
        //gathers commands from all(out)queues until udp-packet is full and sends it!
        //</summary>
        internal override bool SendOutgoingCommands()
        {
            if (base.peerConnectionState == PeerBase.ConnectionStateValue.Disconnected)
            {
                return false;
            }
            if (!base.rt.Connected)
            {
                return false;
            }
            lock (this.udpBuffer)
            {
                NCommand current;
                int num = 0;
                this.udpBufferIndex = 12;
                if (base.crcEnabled)
                {
                    this.udpBufferIndex += 4;
                }
                this.udpCommandCount = 0;
                base.timeInt = SupportClass.GetTickCount() - base.timeBase;
                base.timeLastSendOutgoing = base.timeInt;
                lock (this.outgoingAcknowledgementsList)
                {
                    if (this.outgoingAcknowledgementsList.Count > 0)
                    {
                        num = this.SerializeToBuffer(this.outgoingAcknowledgementsList);
                        base.timeLastSendAck = base.timeInt;
                    }
                }
                if ((!base.IsSendingOnlyAcks && (base.timeInt > base.timeoutInt)) && this.sentReliableCommands.Count > 0)
                {
                    lock (this.sentReliableCommands)
                    {
                        Queue<NCommand> queue = new Queue<NCommand>();
                        using (List<NCommand>.Enumerator enumerator = this.sentReliableCommands.GetEnumerator())
                        {
                            while (enumerator.MoveNext())
                            {
                                current = enumerator.Current;
                                if (current != null && base.timeInt - current.commandSentTime > current.roundTripTimeout)
                                {
                                    if (current.commandSentCount > base.sentCountAllowance || base.timeInt > current.timeoutTime)
                                    {
                                        if (base.debugOut >= DebugLevel.WARNING)
                                        {
                                            base.Listener.DebugReturn(DebugLevel.WARNING, string.Concat(new object[] { "Timeout-disconnect! Command: ", current, " now: ", base.timeInt, " challenge: ", Convert.ToString(this.challenge, 0x10) }));
                                        }
                                        if (base.CommandLog != null)
                                        {
                                            base.CommandLog.Enqueue(new CmdLogSentReliable(current, base.timeInt, base.roundTripTime, base.roundTripTimeVariance, true));
                                            base.CommandLogResize();
                                        }
                                        base.peerConnectionState = PeerBase.ConnectionStateValue.Zombie;
                                        base.Listener.OnStatusChanged(StatusCode.TimeoutDisconnect);
                                        this.Disconnect();
                                        return false;
                                    }
                                    queue.Enqueue(current);
                                }
                            }
                        }
                        while (queue.Count > 0)
                        {
                            current = queue.Dequeue();
                            this.QueueOutgoingReliableCommand(current);
                            this.sentReliableCommands.Remove(current);
                            this.reliableCommandsRepeated++;
                            if (base.debugOut >= DebugLevel.INFO)
                            {
                                base.Listener.DebugReturn(DebugLevel.INFO, string.Format("Resending: {0}. times out after: {1} sent: {3} now: {2} rtt/var: {4}/{5} last recv: {6}", new object[] { current, current.roundTripTimeout, base.timeInt, current.commandSentTime, base.roundTripTime, base.roundTripTimeVariance, SupportClass.GetTickCount() - base.timestampOfLastReceive }));
                            }
                        }
                    }
                }
                if ((((!base.IsSendingOnlyAcks && (base.peerConnectionState == PeerBase.ConnectionStateValue.Connected)) && ((base.timePingInterval > 0) && (this.sentReliableCommands.Count == 0))) && (((base.timeInt - base.timeLastAckReceive) > base.timePingInterval) && !this.AreReliableCommandsInTransit())) && ((this.udpBufferIndex + 12) < this.udpBuffer.Length))
                {
                    current = new NCommand(this, NCommand.CT_PING, null, 0xff);
                    this.QueueOutgoingReliableCommand(current);
                    if (base.TrafficStatsEnabled)
                    {
                        base.TrafficStatsOutgoing.CountControlCommand(current.Size);
                    }
                }
                if (!base.IsSendingOnlyAcks)
                {
                    lock (this.channels)
                    {
                        for (int i = 0; i < this.channelArray.Length; i++)
                        {
                            EnetChannel channel = this.channelArray[i];
                            num += this.SerializeToBuffer(channel.outgoingReliableCommandsList);
                            num += this.SerializeToBuffer(channel.outgoingUnreliableCommandsList);
                        }
                    }
                }
                if (this.udpCommandCount <= 0)
                {
                    return false;
                }
                if (base.TrafficStatsEnabled)
                {
                    base.TrafficStatsOutgoing.TotalPacketCount++;
                    base.TrafficStatsOutgoing.TotalCommandsInPackets += this.udpCommandCount;
                }
                this.SendData(this.udpBuffer, this.udpBufferIndex);
                return (num > 0);
            }
        }
        //<summary> Returns the UDP Payload starting with Magic Number for binary protocol</summary>
        internal override byte[] SerializeOperationToMessage(byte opc, Dictionary<byte, object> parameters, PeerBase.EgMessageType messageType, bool encrypt)
        {
            byte[] buffer;
            lock (base.SerializeMemStream)
            {
                base.SerializeMemStream.Position = 0L;
                base.SerializeMemStream.SetLength(0L);
                if (!encrypt)
                {
                    base.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                }
                Protocol.SerializeOperationRequest(base.SerializeMemStream, opc, parameters, false);
                if (encrypt)
                {
                    byte[] data = base.SerializeMemStream.ToArray();
                    data = base.CryptoProvider.Encrypt(data);
                    base.SerializeMemStream.Position = 0L;
                    base.SerializeMemStream.SetLength(0L);
                    base.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                    base.SerializeMemStream.Write(data, 0, data.Length);
                }
                buffer = base.SerializeMemStream.ToArray();
            }
            if (messageType != PeerBase.EgMessageType.Operation)
            {
                buffer[messageHeader.Length - 1] = (byte)messageType;
            }
            if (encrypt)
            {
                buffer[messageHeader.Length - 1] = (byte)(buffer[messageHeader.Length - 1] | 0x80);
            }
            return buffer;
        }

        internal int SerializeToBuffer(Queue<NCommand> commandList)
        {
            while (commandList.Count > 0)
            {
                NCommand command = commandList.Peek();
                if (command == null)
                {
                    commandList.Dequeue();
                }
                else
                {
                    if (this.udpBufferIndex + command.Size > this.udpBuffer.Length)
                    {
                        if (base.debugOut >= DebugLevel.INFO)
                        {
                            base.Listener.DebugReturn(DebugLevel.INFO, string.Concat(new object[] { "UDP package is full. Commands in Package: ", this.udpCommandCount, ". Commands left in queue: ", commandList.Count }));
                        }
                        break;
                    }
                    Buffer.BlockCopy(command.Serialize(), 0, this.udpBuffer, this.udpBufferIndex, command.Size);
                    this.udpBufferIndex += command.Size;
                    this.udpCommandCount = (byte)(this.udpCommandCount + 1);
                    if ((command.commandFlags & 1) > 0)
                    {
                        this.QueueSentCommand(command);
                        if (base.CommandLog != null)
                        {
                            base.CommandLog.Enqueue(new CmdLogSentReliable(command, base.timeInt, base.roundTripTime, base.roundTripTimeVariance, false));
                            base.CommandLogResize();
                        }
                    }
                    commandList.Dequeue();
                }
            }
            return commandList.Count;
        }

        internal override void StopConnection()
        {
            if (base.rt != null)
            {
                base.rt.Disconnect();
            }
            base.peerConnectionState = PeerBase.ConnectionStateValue.Disconnected;
            if (base.Listener != null)
            {
                base.Listener.OnStatusChanged(StatusCode.Disconnect);
            }
        }

        //<summary>queues incoming commands in the correct order as either unreliable, reliable or unsequenced. return value determines if the command is queued / done.</summary>
        internal override int QueuedIncomingCommandsCount
        {
            get
            {
                int num = 0;
                lock (this.channels)
                {
                    foreach (EnetChannel channel in this.channels.Values)
                    {
                        num += channel.incomingReliableCommandsList.Count;
                        num += channel.incomingUnreliableCommandsList.Count;
                    }
                }
                return num;
            }
        }

        internal override int QueuedOutgoingCommandsCount
        {
            get
            {
                int num = 0;
                lock (this.channels)
                {
                    foreach (EnetChannel channel in this.channels.Values)
                    {
                        num += channel.outgoingReliableCommandsList.Count;
                        num += channel.outgoingUnreliableCommandsList.Count;
                    }
                }
                return num;
            }
        }
    }
}