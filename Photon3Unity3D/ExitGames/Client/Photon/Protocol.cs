﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ExitGames.Client.Photon
{
    //<summary>
    //Provides tools for the Exit Games Protocol
    //</summary>
    public class Protocol
    {
        internal static readonly Dictionary<byte, CustomType> CodeDict = new Dictionary<byte, CustomType>();
        private static readonly byte[] memDeserialize = new byte[4];
        private static readonly byte[] memDouble = new byte[8];
        private static readonly double[] memDoubleBlock = new double[1];
        private static readonly byte[] memDoubleBlockBytes = new byte[8];
        private static readonly byte[] memFloat = new byte[4];
        private static readonly float[] memFloatBlock = new float[1];
        private static readonly byte[] memFloatBlockBytes = new byte[4];
        private static readonly byte[] memInteger = new byte[4];
        private static readonly byte[] memLong = new byte[8];
        private static readonly long[] memLongBlock = new long[1];
        private static readonly byte[] memLongBlockBytes = new byte[8];
        private static readonly byte[] memShort = new byte[2];
        public const string protocolType = "GpBinaryV16";
        internal static readonly Dictionary<Type, CustomType> TypeDict = new Dictionary<Type, CustomType>();

        private static Array CreateArrayByType(byte arrayType, short length) =>
            Array.CreateInstance(GetTypeOfCode(arrayType), length);
        //<summary>
        //Deserialize returns an object reassembled from the given byte-array.
        //</summary>
        //<param name = "serializedData" > The byte-array to be Deserialized</param>
        //<returns>The Deserialized object</returns>
        public static object Deserialize(byte[] serializedData)
        {
            MemoryStream din = new MemoryStream(serializedData);
            return Deserialize(din, (byte)din.ReadByte());
        }

        private static object Deserialize(MemoryStream din, byte type)
        {
            switch (type)
            {
                case (byte)GpType.StringArray:
                    return DeserializeStringArray(din);

                case (byte)GpType.Byte:
                    return DeserializeByte(din);

                case (byte)GpType.Custom:
                    {
                        byte customTypeCode = (byte)din.ReadByte();
                        return DeserializeCustom(din, customTypeCode);
                    }
                case (byte)GpType.Double:
                    return DeserializeDouble(din);

                case (byte)GpType.EventData:
                    return DeserializeEventData(din);

                case (byte)GpType.Float:
                    return DeserializeFloat(din);

                case (byte)GpType.Hashtable:
                    return DeserializeHashTable(din);

                case (byte)GpType.Integer:
                    return DeserializeInteger(din);

                case (byte)GpType.Short:
                    return DeserializeShort(din);

                case (byte)GpType.Long:
                    return DeserializeLong(din);

                case (byte)GpType.IntegerArray:
                    return DeserializeIntArray(din);

                case (byte)GpType.Boolean:
                    return DeserializeBoolean(din);

                case (byte)GpType.OperationResponse:
                    return DeserializeOperationResponse(din);

                case (byte)GpType.OperationRequest:
                    return DeserializeOperationRequest(din);

                case (byte)GpType.String:
                    return DeserializeString(din);

                case (byte)GpType.ByteArray:
                    return DeserializeByteArray(din);

                case (byte)GpType.Array:
                    return DeserializeArray(din);

                case (byte)GpType.ObjectArray:
                    return DeserializeObjectArray(din);

                case (byte)GpType.Dictionary:
                    return DeserializeDictionary(din);

                case (byte)GpType.Unknown:
                case (byte)GpType.Null:
                    return null;
            }
            Debug.WriteLine("missing type: " + type);
            throw new Exception("deserialize(): " + type);
        }
        // <summary>
        // Deserialize fills the given short typed value with the given byte-array(source) starting at the also given offset.
        //The result is placed in a variable (value). There is no need to return a value because the parameter value is given by reference.
        //The altered offset is this way also known to the caller.
        // </summary>
        // <param name = "value" > The short value to deserialized into</param>
        // <param name = "source" > The byte-array to deserialize from</param>
        // <param name = "offset" > The offset in the byte-array</param>
        public static void Deserialize(out short value, byte[] source, ref int offset)
        {
            value = (short)((source[offset++] << 8) | source[offset++]);
        }
        // <summary>
        // Deserialize fills the given int typed value with the given byte-array(source) starting at the also given offset.
        //The result is placed in a variable (value). There is no need to return a value because the parameter value is given by reference.
        //The altered offset is this way also known to the caller.
        // </summary>
        // <param name = "value" > The int value to deserialize into</param>
        // <param name = "source" > The byte-array to deserialize from</param>
        // <param name = "offset" > The offset in the byte-array</param>
        public static void Deserialize(out int value, byte[] source, ref int offset)
        {
            value = (((source[offset++] << 0x18) | (source[offset++] << 0x10)) | (source[offset++] << 8)) | source[offset++];
        }
        // <summary>
        // Deserialize fills the given float typed value with the given byte-array(source) starting at the also given offset.
        //The result is placed in a variable (value). There is no need to return a value because the parameter value is given by reference.
        //The altered offset is this way also known to the caller.
        // </summary>
        // <param name = "value" > The float value to deserialize</param>
        // <param name = "source" > The byte-array to deserialize from</param>
        // <param name = "offset" > The offset in the byte-array</param>
        public static void Deserialize(out float value, byte[] source, ref int offset)
        {
            if (BitConverter.IsLittleEndian)
            {
                lock (Protocol.memDeserialize)
                {
                    byte[] memDeserialize = Protocol.memDeserialize;
                    memDeserialize[3] = source[offset++];
                    memDeserialize[2] = source[offset++];
                    memDeserialize[1] = source[offset++];
                    memDeserialize[0] = source[offset++];
                    value = BitConverter.ToSingle(memDeserialize, 0);
                }
            }
            else
            {
                value = BitConverter.ToSingle(source, offset);
                offset += 4;
            }
        }

        private static Array DeserializeArray(MemoryStream din)
        {
            Array array;
            Array array2;
            short num3;
            short length = DeserializeShort(din);
            byte arrayType = (byte)din.ReadByte();
            switch (arrayType)
            {
                case (byte)GpType.Array:
                    array2 = DeserializeArray(din);
                    array = Array.CreateInstance(array2.GetType(), length);
                    array.SetValue(array2, 0);
                    for (num3 = 1; num3 < length; num3 = (short)(num3 + 1))
                    {
                        array2 = DeserializeArray(din);
                        array.SetValue(array2, (int)num3);
                    }
                    return array;

                case (byte)GpType.ByteArray:
                    array = Array.CreateInstance(typeof(byte[]), length);
                    for (num3 = 0; num3 < length; num3 = (short)(num3 + 1))
                    {
                        array2 = DeserializeByteArray(din);
                        array.SetValue(array2, (int)num3);
                    }
                    return array;

                case (byte)GpType.Custom:
                    {
                        byte key = (byte)din.ReadByte();
                        if (!CodeDict.TryGetValue(key, out CustomType type2))
                        {
                            throw new Exception("Cannot find deserializer for custom type: " + key);
                        }
                        array = Array.CreateInstance(type2.Type, length);
                        for (int i = 0; i < length; i++)
                        {
                            short count = DeserializeShort(din);
                            if (type2.DeserializeStreamFunction == null)
                            {
                                byte[] buffer = new byte[count];
                                din.Read(buffer, 0, count);
                                array.SetValue(type2.DeserializeFunction(buffer), i);
                            }
                            else
                            {
                                array.SetValue(type2.DeserializeStreamFunction(din, count), i);
                            }
                        }
                        return array;
                    }
                case (byte)GpType.Dictionary:
                    {
                        DeserializeDictionaryArray(din, length, out Array arrayResult);
                        return arrayResult;
                    }
            }
            array = CreateArrayByType(arrayType, length);
            for (num3 = 0; num3 < length; num3 = (short)(num3 + 1))
            {
                array.SetValue(Deserialize(din, arrayType), (int)num3);
            }
            return array;
        }

        private static bool DeserializeBoolean(MemoryStream din) =>
            (din.ReadByte() != 0);

        private static byte DeserializeByte(MemoryStream din) =>
            ((byte)din.ReadByte());

        private static byte[] DeserializeByteArray(MemoryStream din)
        {
            int count = DeserializeInteger(din);
            byte[] buffer = new byte[count];
            din.Read(buffer, 0, count);
            return buffer;
        }

        private static object DeserializeCustom(MemoryStream din, byte customTypeCode)
        {
            short count = DeserializeShort(din);
            if (CodeDict.TryGetValue(customTypeCode, out CustomType type))
            {
                if (type.DeserializeStreamFunction == null)
                {
                    byte[] buffer = new byte[count];
                    din.Read(buffer, 0, count);
                    return type.DeserializeFunction(buffer);
                }
                long position = din.Position;
                object obj2 = type.DeserializeStreamFunction(din, count);
                int num3 = (int)(din.Position - position);
                if (num3 != count)
                {
                    din.Position = position + count;
                }
                return obj2;
            }
            return null;
        }

        private static IDictionary DeserializeDictionary(MemoryStream din)
        {
            byte typeCode = (byte)din.ReadByte();
            byte num2 = (byte)din.ReadByte();
            int num3 = DeserializeShort(din);
            bool flag = (typeCode == 0) || (typeCode == 0x2a);
            bool flag2 = (num2 == 0) || (num2 == 0x2a);
            Type typeOfCode = GetTypeOfCode(typeCode);
            Type type2 = GetTypeOfCode(num2);
            IDictionary dictionary = Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(new Type[] { typeOfCode, type2 })) as IDictionary;
            for (int i = 0; i < num3; i++)
            {
                object key = Deserialize(din, flag ? ((byte)din.ReadByte()) : typeCode);
                object obj3 = Deserialize(din, flag2 ? ((byte)din.ReadByte()) : num2);
                dictionary.Add(key, obj3);
            }
            return dictionary;
        }

        private static bool DeserializeDictionaryArray(MemoryStream din, short size, out Array arrayResult)
        {
            Type elementType = DeserializeDictionaryType(din, out byte keyTypeCode, out byte valueTypeCode);
            arrayResult = Array.CreateInstance(elementType, size);
            for (short i = 0; i < size; i = (short)(i + 1))
            {
                IDictionary dictionary = Activator.CreateInstance(elementType) as IDictionary;
                if (dictionary == null)
                {
                    return false;
                }
                short len = DeserializeShort(din);
                for (int j = 0; j < len; j++)
                {
                    object keyObj;
                    byte num6;
                    object valueObj;
                    if (keyTypeCode != 0)
                    {
                        keyObj = Deserialize(din, keyTypeCode);
                    }
                    else
                    {
                        num6 = (byte)din.ReadByte();
                        keyObj = Deserialize(din, num6);
                    }
                    if (valueTypeCode != 0)
                    {
                        valueObj = Deserialize(din, valueTypeCode);
                    }
                    else
                    {
                        num6 = (byte)din.ReadByte();
                        valueObj = Deserialize(din, num6);
                    }
                    dictionary.Add(keyObj, valueObj);
                }
                arrayResult.SetValue(dictionary, (int)i);
            }
            return true;
        }

        private static Type DeserializeDictionaryType(MemoryStream reader, out byte keyTypeCode, out byte valTypeCode)
        {
            Type typeOfCode;
            Type type4;
            keyTypeCode = (byte)reader.ReadByte();
            valTypeCode = (byte)reader.ReadByte();
            GpType type = (GpType)keyTypeCode;
            GpType type2 = (GpType)valTypeCode;
            if (type == GpType.Unknown)
            {
                typeOfCode = typeof(object);
            }
            else
            {
                typeOfCode = GetTypeOfCode(keyTypeCode);
            }
            if (type2 == GpType.Unknown)
            {
                type4 = typeof(object);
            }
            else
            {
                type4 = GetTypeOfCode(valTypeCode);
            }
            return typeof(Dictionary<,>).MakeGenericType(new Type[] { typeOfCode, type4 });
        }

        private static double DeserializeDouble(MemoryStream din)
        {
            lock (Protocol.memDouble)
            {
                byte[] memDouble = Protocol.memDouble;
                din.Read(memDouble, 0, 8);
                if (BitConverter.IsLittleEndian)
                {
                    byte num = memDouble[0];
                    byte num2 = memDouble[1];
                    byte num3 = memDouble[2];
                    byte num4 = memDouble[3];
                    memDouble[0] = memDouble[7];
                    memDouble[1] = memDouble[6];
                    memDouble[2] = memDouble[5];
                    memDouble[3] = memDouble[4];
                    memDouble[4] = num4;
                    memDouble[5] = num3;
                    memDouble[6] = num2;
                    memDouble[7] = num;
                }
                return BitConverter.ToDouble(memDouble, 0);
            }
        }

        internal static EventData DeserializeEventData(MemoryStream din) =>
            new EventData
            {
                Code = DeserializeByte(din),
                Parameters = DeserializeParameterTable(din)
            };

        private static float DeserializeFloat(MemoryStream din)
        {
            lock (Protocol.memFloat)
            {
                byte[] memFloat = Protocol.memFloat;
                din.Read(memFloat, 0, 4);
                if (BitConverter.IsLittleEndian)
                {
                    byte num = memFloat[0];
                    byte num2 = memFloat[1];
                    memFloat[0] = memFloat[3];
                    memFloat[1] = memFloat[2];
                    memFloat[2] = num2;
                    memFloat[3] = num;
                }
                return BitConverter.ToSingle(memFloat, 0);
            }
        }

        private static Hashtable DeserializeHashTable(MemoryStream din)
        {
            int x = DeserializeShort(din);
            Hashtable hashtable = new Hashtable(x);
            for (int i = 0; i < x; i++)
            {
                object key = Deserialize(din, (byte)din.ReadByte());
                object value = Deserialize(din, (byte)din.ReadByte());
                hashtable[key] = value;
            }
            return hashtable;
        }

        private static int[] DeserializeIntArray(MemoryStream din)
        {
            int num = DeserializeInteger(din);
            int[] numArray = new int[num];
            for (int i = 0; i < num; i++)
            {
                numArray[i] = DeserializeInteger(din);
            }
            return numArray;
        }
        //<summary>
        //DeserializeInteger returns an Integer typed value from the given Memorystream.
        //</summary>
        private static int DeserializeInteger(MemoryStream din)
        {
            lock (Protocol.memInteger)
            {
                byte[] memInteger = Protocol.memInteger;
                din.Read(memInteger, 0, 4);
                return ((((memInteger[0] << 0x18) | (memInteger[1] << 0x10)) | (memInteger[2] << 8)) | memInteger[3]);
            }
        }

        private static long DeserializeLong(MemoryStream din)
        {
            lock (Protocol.memLong)
            {
                byte[] memLong = Protocol.memLong;
                din.Read(memLong, 0, 8);
                if (BitConverter.IsLittleEndian)
                {
                    return (long)((((((((memLong[0] << 0x38) | (memLong[1] << 0x30)) | (memLong[2] << 40)) | (memLong[3] << 0x20)) | (memLong[4] << 0x18)) | (memLong[5] << 0x10)) | (memLong[6] << 8)) | memLong[7]);
                }
                return BitConverter.ToInt64(memLong, 0);
            }
        }

        internal static object DeserializeMessage(MemoryStream stream) =>
            Deserialize(stream, (byte)stream.ReadByte());

        private static object[] DeserializeObjectArray(MemoryStream din)
        {
            short num = DeserializeShort(din);
            object[] objArray = new object[num];
            for (int i = 0; i < num; i++)
            {
                byte type = (byte)din.ReadByte();
                objArray[i] = Deserialize(din, type);
            }
            return objArray;
        }

        internal static OperationRequest DeserializeOperationRequest(MemoryStream din) =>
            new OperationRequest
            {
                OperationCode = DeserializeByte(din),
                Parameters = DeserializeParameterTable(din)
            };

        internal static OperationResponse DeserializeOperationResponse(MemoryStream memoryStream) =>
            new OperationResponse
            {
                OperationCode = DeserializeByte(memoryStream),
                ReturnCode = DeserializeShort(memoryStream),
                DebugMessage = Deserialize(memoryStream, DeserializeByte(memoryStream)) as string,
                Parameters = DeserializeParameterTable(memoryStream)
            };

        private static Dictionary<byte, object> DeserializeParameterTable(MemoryStream memoryStream)
        {
            short capacity = DeserializeShort(memoryStream);
            Dictionary<byte, object> dictionary = new Dictionary<byte, object>(capacity);
            for (int i = 0; i < capacity; i++)
            {
                byte key = (byte)memoryStream.ReadByte();
                object value = Deserialize(memoryStream, (byte)memoryStream.ReadByte());
                dictionary[key] = value;
            }
            return dictionary;
        }

        internal static byte[] DeserializeRawMessage(MemoryStream stream) =>
            ((byte[])Deserialize(stream, (byte)stream.ReadByte()));

        private static short DeserializeShort(MemoryStream din)
        {
            lock (Protocol.memShort)
            {
                byte[] memShort = Protocol.memShort;
                din.Read(memShort, 0, 2);
                return (short)((memShort[0] << 8) | memShort[1]);
            }
        }

        private static string DeserializeString(MemoryStream din)
        {
            short num = DeserializeShort(din);
            if (num == 0)
            {
                return "";
            }
            byte[] buffer = new byte[num];
            din.Read(buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }

        private static string[] DeserializeStringArray(MemoryStream din)
        {
            int num = DeserializeShort(din);
            string[] strArray = new string[num];
            for (int i = 0; i < num; i++)
            {
                strArray[i] = DeserializeString(din);
            }
            return strArray;
        }

        private static GpType GetCodeOfType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                    return GpType.Boolean;

                case TypeCode.Byte:
                    return GpType.Byte;

                case TypeCode.Int16:
                    return GpType.Short;

                case TypeCode.Int32:
                    return GpType.Integer;

                case TypeCode.Int64:
                    return GpType.Long;

                case TypeCode.Single:
                    return GpType.Float;

                case TypeCode.Double:
                    return GpType.Double;

                case TypeCode.String:
                    return GpType.String;
            }
            if (type.IsArray)
            {
                if (type == typeof(byte[]))
                {
                    return GpType.ByteArray;
                }
                return GpType.Array;
            }
            if (type == typeof(Hashtable))
            {
                return GpType.Hashtable;
            }
            if (type.IsGenericType && (typeof(Dictionary<,>) == type.GetGenericTypeDefinition()))
            {
                return GpType.Dictionary;
            }
            if (type == typeof(EventData))
            {
                return GpType.EventData;
            }
            if (type == typeof(OperationRequest))
            {
                return GpType.OperationRequest;
            }
            if (type == typeof(OperationResponse))
            {
                return GpType.OperationResponse;
            }
            return GpType.Unknown;
        }

        private static Type GetTypeOfCode(byte typeCode)
        {
            switch (typeCode)
            {
                case (byte)GpType.StringArray:
                    return typeof(string[]);

                case (byte)GpType.Byte:
                    return typeof(byte);

                case (byte)GpType.Custom:
                    return typeof(CustomType);

                case (byte)GpType.Double:
                    return typeof(double);

                case (byte)GpType.EventData:
                    return typeof(EventData);

                case (byte)GpType.Float:
                    return typeof(float);

                case (byte)GpType.Hashtable:
                    return typeof(Hashtable);

                case (byte)GpType.Integer:
                    return typeof(int);

                case (byte)GpType.Short:
                    return typeof(short);

                case (byte)GpType.Long:
                    return typeof(long);

                case (byte)GpType.IntegerArray:
                    return typeof(int[]);

                case (byte)GpType.Boolean:
                    return typeof(bool);

                case (byte)GpType.OperationResponse:
                    return typeof(OperationResponse);

                case (byte)GpType.OperationRequest:
                    return typeof(OperationRequest);

                case (byte)GpType.String:
                    return typeof(string);

                case (byte)GpType.ByteArray:
                    return typeof(byte[]);

                case (byte)GpType.Array:
                    return typeof(Array);

                case (byte)GpType.ObjectArray:
                    return typeof(object[]);

                case (byte)GpType.Dictionary:
                    return typeof(IDictionary);

                case (byte)GpType.Unknown:
                case (byte)GpType.Null:
                    return typeof(object);
            }
            Debug.WriteLine("missing type: " + typeCode);
            throw new Exception("deserialize(): " + typeCode);
        }
        //<summary>
        //Serialize creates a byte-array from the given object and returns it.
        //</summary>
        //<param name = "obj" > The object to serialize</param>
        //<returns>The serialized byte-array</returns>
        public static byte[] Serialize(object obj)
        {
            MemoryStream dout = new MemoryStream(0x40);
            Serialize(dout, obj, true);
            return dout.ToArray();
        }
        // <summary>
        // Serializes a short typed value into a byte-array(target) starting at the also given targetOffset.
        //The altered offset is known to the caller, because it is given via a referenced parameter.
        // </summary>
        // <param name = "value" > The short value to be serialized</param>
        // <param name = "target" > The byte-array to serialize the short to</param>
        // <param name = "targetOffset" > The offset in the byte-array</param>
        public static void Serialize(short value, byte[] target, ref int targetOffset)
        {
            target[targetOffset++] = (byte)(value >> 8);
            target[targetOffset++] = (byte)value;
        }

        public static void Serialize(int value, byte[] target, ref int targetOffset)
        {
            target[targetOffset++] = (byte)(value >> 0x18);
            target[targetOffset++] = (byte)(value >> 0x10);
            target[targetOffset++] = (byte)(value >> 8);
            target[targetOffset++] = (byte)value;
        }
        //<summary>
        //Calls the correct serialization method for the passed object.
        //</summary>
        private static void Serialize(MemoryStream dout, object serObject, bool setType)
        {
            if (serObject == null)
            {
                if (setType)
                {
                    dout.WriteByte((byte)GpType.Null);
                }
            }
            else
            {
                switch (GetCodeOfType(serObject.GetType()))
                {
                    case GpType.Byte:
                        SerializeByte(dout, (byte)serObject, setType);
                        return;

                    case GpType.Double:
                        SerializeDouble(dout, (double)serObject, setType);
                        return;

                    case GpType.EventData:
                        SerializeEventData(dout, (EventData)serObject, setType);
                        return;

                    case GpType.Float:
                        SerializeFloat(dout, (float)serObject, setType);
                        return;

                    case GpType.Hashtable:
                        SerializeHashTable(dout, (Hashtable)serObject, setType);
                        return;

                    case GpType.Integer:
                        SerializeInteger(dout, (int)serObject, setType);
                        return;

                    case GpType.Short:
                        SerializeShort(dout, (short)serObject, setType);
                        return;

                    case GpType.Long:
                        SerializeLong(dout, (long)serObject, setType);
                        return;

                    case GpType.Boolean:
                        SerializeBoolean(dout, (bool)serObject, setType);
                        return;

                    case GpType.OperationResponse:
                        SerializeOperationResponse(dout, (OperationResponse)serObject, setType);
                        return;

                    case GpType.OperationRequest:
                        SerializeOperationRequest(dout, (OperationRequest)serObject, setType);
                        return;

                    case GpType.String:
                        SerializeString(dout, (string)serObject, setType);
                        return;

                    case GpType.ByteArray:
                        SerializeByteArray(dout, (byte[])serObject, setType);
                        return;

                    case GpType.Array:
                        if (!(serObject is int[]))
                        {
                            if (serObject.GetType().GetElementType() == typeof(object))
                            {
                                SerializeObjectArray(dout, serObject as object[], setType);
                            }
                            else
                            {
                                SerializeArray(dout, (Array)serObject, setType);
                            }
                            return;
                        }
                        SerializeIntArrayOptimized(dout, (int[])serObject, setType);
                        return;

                    case GpType.Dictionary:
                        SerializeDictionary(dout, (IDictionary)serObject, setType);
                        return;
                }
                if (!SerializeCustom(dout, serObject))
                {
                    throw new Exception("cannot serialize(): " + serObject.GetType());
                }
            }
        }
        // <summary>
        // Serializes an float typed value into a byte-array(target) starting at the also given targetOffset.
        //The altered offset is known to the caller, because it is given via a referenced parameter.
        // </summary>
        // <param name = "value" > The float value to be serialized</param>
        // <param name = "target" > The byte-array to serialize the short to</param>
        // <param name = "targetOffset" > The offset in the byte-array</param>
        public static void Serialize(float value, byte[] target, ref int targetOffset)
        {
            lock (memFloatBlock)
            {
                memFloatBlock[0] = value;
                Buffer.BlockCopy(memFloatBlock, 0, target, targetOffset, 4);
            }
            if (BitConverter.IsLittleEndian)
            {
                byte num = target[targetOffset];
                byte num2 = target[targetOffset + 1];
                target[targetOffset] = target[targetOffset + 3];
                target[targetOffset + 1] = target[targetOffset + 2];
                target[targetOffset + 2] = num2;
                target[targetOffset + 3] = num;
            }
            targetOffset += 4;
        }

        private static void SerializeArray(MemoryStream dout, Array serObject, bool setType)
        {
            int num;
            if (setType)
            {
                dout.WriteByte((byte)GpType.Array);
            }
            if (serObject.Length > 0x7fff)
            {
                throw new NotSupportedException("String[] that exceed 32767 (short.MaxValue) entries are not supported. Yours is: " + serObject.Length);
            }
            SerializeShort(dout, (short)serObject.Length, false);
            Type elementType = serObject.GetType().GetElementType();
            GpType codeOfType = GetCodeOfType(elementType);
            if (codeOfType != GpType.Unknown)
            {
                dout.WriteByte((byte)codeOfType);
                if (codeOfType == GpType.Dictionary)
                {
                    SerializeDictionaryHeader(dout, serObject, out bool setKeyType, out bool setValueType);
                    for (num = 0; num < serObject.Length; num++)
                    {
                        object dict = serObject.GetValue(num);
                        SerializeDictionaryElements(dout, dict, setKeyType, setValueType);
                    }
                }
                else
                {
                    num = 0;
                    while (num < serObject.Length)
                    {
                        object serObjectValue = serObject.GetValue(num);
                        Serialize(dout, serObjectValue, false);
                        num++;
                    }
                }
            }
            else
            {
                if (!TypeDict.TryGetValue(elementType, out CustomType customType))
                {
                    throw new NotSupportedException("cannot serialize array of type " + elementType);
                }
                dout.WriteByte((byte)GpType.Custom);
                dout.WriteByte(customType.Code);
                for (num = 0; num < serObject.Length; num++)
                {
                    object customObject = serObject.GetValue(num);
                    if (customType.SerializeStreamFunction == null)
                    {
                        byte[] buffer = customType.SerializeFunction(customObject);
                        SerializeShort(dout, (short)buffer.Length, false);
                        dout.Write(buffer, 0, buffer.Length);
                    }
                    else
                    {
                        long position = dout.Position;
                        dout.Position += 2L;
                        short num3 = customType.SerializeStreamFunction(dout, customObject);
                        long pos = dout.Position;
                        dout.Position = position;
                        SerializeShort(dout, num3, false);
                        dout.Position += num3;
                        if (dout.Position != pos)
                        {
                            throw new Exception(string.Concat(new object[] { "Serialization failed. Stream position corrupted. Should be ", pos, " is now: ", dout.Position, " serializedLength: ", num3 }));
                        }
                    }
                }
            }
        }

        private static void SerializeBoolean(MemoryStream dout, bool serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Boolean);
            }
            dout.WriteByte(serObject ? ((byte)1) : ((byte)0));
        }

        private static void SerializeByte(MemoryStream dout, byte serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Byte);
            }
            dout.WriteByte(serObject);
        }

        private static void SerializeByteArray(MemoryStream dout, byte[] serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.ByteArray);
            }
            SerializeInteger(dout, serObject.Length, false);
            dout.Write(serObject, 0, serObject.Length);
        }

        private static bool SerializeCustom(MemoryStream dout, object serObject)
        {
            if (TypeDict.TryGetValue(serObject.GetType(), out CustomType type))
            {
                if (type.SerializeStreamFunction == null)
                {
                    byte[] buffer = type.SerializeFunction(serObject);
                    dout.WriteByte((byte)GpType.Custom);
                    dout.WriteByte(type.Code);
                    SerializeShort(dout, (short)buffer.Length, false);
                    dout.Write(buffer, 0, buffer.Length);
                    return true;
                }
                dout.WriteByte((byte)GpType.Custom);
                dout.WriteByte(type.Code);
                long position = dout.Position;
                dout.Position += 2L;
                short num2 = type.SerializeStreamFunction(dout, serObject);
                long num3 = dout.Position;
                dout.Position = position;
                SerializeShort(dout, num2, false);
                dout.Position += num2;
                if (dout.Position != num3)
                {
                    throw new Exception(string.Concat(new object[] { "Serialization failed. Stream position corrupted. Should be ", num3, " is now: ", dout.Position, " serializedLength: ", num2 }));
                }
                return true;
            }
            return false;
        }

        private static void SerializeDictionary(MemoryStream dout, IDictionary serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Dictionary);
            }
            SerializeDictionaryHeader(dout, serObject, out bool setKeyType, out bool setValueType);
            SerializeDictionaryElements(dout, serObject, setKeyType, setValueType);
        }

        private static void SerializeDictionaryElements(MemoryStream writer, object dict, bool setKeyType, bool setValueType)
        {
            IDictionary dictionary = (IDictionary)dict;
            SerializeShort(writer, (short)dictionary.Count, false);
            foreach (DictionaryEntry entry in dictionary)
            {
                if (!(setValueType || entry.Value != null))
                {
                    throw new Exception("Can't serialize null in Dictionary with specific value-type.");
                }
                if (!(setKeyType || entry.Key != null))
                {
                    throw new Exception("Can't serialize null in Dictionary with specific key-type.");
                }
                Serialize(writer, entry.Key, setKeyType);
                Serialize(writer, entry.Value, setValueType);
            }
        }

        private static void SerializeDictionaryHeader(MemoryStream writer, Type dictType)
        {
            SerializeDictionaryHeader(writer, dictType, out bool setKeyType, out bool setValueType);
        }

        private static void SerializeDictionaryHeader(MemoryStream writer, object dict, out bool setKeyType, out bool setValueType)
        {
            Type[] genericArguments = dict.GetType().GetGenericArguments();
            setKeyType = genericArguments[0] == typeof(object);
            setValueType = genericArguments[1] == typeof(object);
            if (setKeyType)
            {
                writer.WriteByte(0);
            }
            else
            {
                GpType codeOfType = GetCodeOfType(genericArguments[0]);
                switch (codeOfType)
                {
                    case GpType.Unknown:
                    case GpType.Dictionary:
                        throw new Exception("Unexpected - cannot serialize Dictionary with key type: " + genericArguments[0]);
                }
                writer.WriteByte((byte)codeOfType);
            }
            if (setValueType)
            {
                writer.WriteByte(0);
            }
            else
            {
                GpType codeOfType = GetCodeOfType(genericArguments[1]);
                if (codeOfType == GpType.Unknown)
                {
                    throw new Exception("Unexpected - cannot serialize Dictionary with value type: " + genericArguments[0]);
                }
                writer.WriteByte((byte)codeOfType);
                if (codeOfType == GpType.Dictionary)
                {
                    SerializeDictionaryHeader(writer, genericArguments[1]);
                }
            }
        }

        private static void SerializeDouble(MemoryStream dout, double serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Double);
            }
            lock (Protocol.memDoubleBlockBytes)
            {
                memDoubleBlock[0] = serObject;
                Buffer.BlockCopy(memDoubleBlock, 0, Protocol.memDoubleBlockBytes, 0, 8);
                byte[] memDoubleBlockBytes = Protocol.memDoubleBlockBytes;
                if (BitConverter.IsLittleEndian)
                {
                    byte num = memDoubleBlockBytes[0];
                    byte num2 = memDoubleBlockBytes[1];
                    byte num3 = memDoubleBlockBytes[2];
                    byte num4 = memDoubleBlockBytes[3];
                    memDoubleBlockBytes[0] = memDoubleBlockBytes[7];
                    memDoubleBlockBytes[1] = memDoubleBlockBytes[6];
                    memDoubleBlockBytes[2] = memDoubleBlockBytes[5];
                    memDoubleBlockBytes[3] = memDoubleBlockBytes[4];
                    memDoubleBlockBytes[4] = num4;
                    memDoubleBlockBytes[5] = num3;
                    memDoubleBlockBytes[6] = num2;
                    memDoubleBlockBytes[7] = num;
                }
                dout.Write(memDoubleBlockBytes, 0, 8);
            }
        }

        internal static void SerializeEventData(MemoryStream memStream, EventData serObject, bool setType)
        {
            if (setType)
            {
                memStream.WriteByte((byte)GpType.EventData);
            }
            memStream.WriteByte(serObject.Code);
            SerializeParameterTable(memStream, serObject.Parameters);
        }

        private static void SerializeFloat(MemoryStream dout, float serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Float);
            }
            lock (memFloatBlockBytes)
            {
                memFloatBlock[0] = serObject;
                Buffer.BlockCopy(memFloatBlock, 0, memFloatBlockBytes, 0, 4);
                if (BitConverter.IsLittleEndian)
                {
                    byte num = memFloatBlockBytes[0];
                    byte num2 = memFloatBlockBytes[1];
                    memFloatBlockBytes[0] = memFloatBlockBytes[3];
                    memFloatBlockBytes[1] = memFloatBlockBytes[2];
                    memFloatBlockBytes[2] = num2;
                    memFloatBlockBytes[3] = num;
                }
                dout.Write(memFloatBlockBytes, 0, 4);
            }
        }

        private static void SerializeHashTable(MemoryStream dout, Hashtable serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Hashtable);
            }
            SerializeShort(dout, (short)serObject.Count, false);
            foreach (DictionaryEntry entry in serObject)
            {
                Serialize(dout, entry.Key, true);
                Serialize(dout, entry.Value, true);
            }
        }

        private static void SerializeIntArrayOptimized(MemoryStream inWriter, int[] serObject, bool setType)
        {
            if (setType)
            {
                inWriter.WriteByte((byte)GpType.Array);
            }
            SerializeShort(inWriter, (short)serObject.Length, false);
            inWriter.WriteByte((byte)GpType.Integer);
            byte[] buffer = new byte[serObject.Length * 4];
            int num = 0;
            for (int i = 0; i < serObject.Length; i++)
            {
                buffer[num++] = (byte)(serObject[i] >> 0x18);
                buffer[num++] = (byte)(serObject[i] >> 0x10);
                buffer[num++] = (byte)(serObject[i] >> 8);
                buffer[num++] = (byte)serObject[i];
            }
            inWriter.Write(buffer, 0, buffer.Length);
        }

        private static void SerializeInteger(MemoryStream dout, int serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Integer);
            }
            lock (Protocol.memInteger)
            {
                byte[] memInteger = Protocol.memInteger;
                memInteger[0] = (byte)(serObject >> 0x18);
                memInteger[1] = (byte)(serObject >> 0x10);
                memInteger[2] = (byte)(serObject >> 8);
                memInteger[3] = (byte)serObject;
                dout.Write(memInteger, 0, 4);
            }
        }

        private static void SerializeLong(MemoryStream dout, long serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Long);
            }
            lock (memLongBlock)
            {
                memLongBlock[0] = serObject;
                Buffer.BlockCopy(memLongBlock, 0, Protocol.memLongBlockBytes, 0, 8);
                byte[] memLongBlockBytes = Protocol.memLongBlockBytes;
                if (BitConverter.IsLittleEndian)
                {
                    byte num = memLongBlockBytes[0];
                    byte num2 = memLongBlockBytes[1];
                    byte num3 = memLongBlockBytes[2];
                    byte num4 = memLongBlockBytes[3];
                    memLongBlockBytes[0] = memLongBlockBytes[7];
                    memLongBlockBytes[1] = memLongBlockBytes[6];
                    memLongBlockBytes[2] = memLongBlockBytes[5];
                    memLongBlockBytes[3] = memLongBlockBytes[4];
                    memLongBlockBytes[4] = num4;
                    memLongBlockBytes[5] = num3;
                    memLongBlockBytes[6] = num2;
                    memLongBlockBytes[7] = num;
                }
                dout.Write(memLongBlockBytes, 0, 8);
            }
        }

        internal static void SerializeMessage(MemoryStream ms, object msg)
        {
            Serialize(ms, msg, true);
        }

        private static void SerializeObjectArray(MemoryStream dout, object[] objects, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.ObjectArray);
            }
            SerializeShort(dout, (short)objects.Length, false);
            for (int i = 0; i < objects.Length; i++)
            {
                object serObject = objects[i];
                Serialize(dout, serObject, true);
            }
        }

        internal static void SerializeOperationRequest(MemoryStream memStream, OperationRequest serObject, bool setType)
        {
            SerializeOperationRequest(memStream, serObject.OperationCode, serObject.Parameters, setType);
        }

        internal static void SerializeOperationRequest(MemoryStream memStream, byte operationCode, Dictionary<byte, object> parameters, bool setType)
        {
            if (setType)
            {
                memStream.WriteByte((byte)GpType.OperationRequest);
            }
            memStream.WriteByte(operationCode);
            SerializeParameterTable(memStream, parameters);
        }

        internal static void SerializeOperationResponse(MemoryStream memStream, OperationResponse serObject, bool setType)
        {
            if (setType)
            {
                memStream.WriteByte((byte)GpType.OperationResponse);
            }
            memStream.WriteByte(serObject.OperationCode);
            SerializeShort(memStream, serObject.ReturnCode, false);
            if (string.IsNullOrEmpty(serObject.DebugMessage))
            {
                memStream.WriteByte((byte)GpType.Null);
            }
            else
            {
                SerializeString(memStream, serObject.DebugMessage, false);
            }
            SerializeParameterTable(memStream, serObject.Parameters);
        }

        private static void SerializeParameterTable(MemoryStream memStream, Dictionary<byte, object> parameters)
        {
            if ((parameters == null) || (parameters.Count == 0))
            {
                SerializeShort(memStream, 0, false);
            }
            else
            {
                SerializeShort(memStream, (short)parameters.Count, false);
                foreach (KeyValuePair<byte, object> pair in parameters)
                {
                    memStream.WriteByte(pair.Key);
                    Serialize(memStream, pair.Value, true);
                }
            }
        }

        private static void SerializeShort(MemoryStream dout, short serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.Short);
            }
            lock (Protocol.memShort)
            {
                byte[] memShort = Protocol.memShort;
                memShort[0] = (byte)(serObject >> 8);
                memShort[1] = (byte)serObject;
                dout.Write(memShort, 0, 2);
            }
        }

        private static void SerializeString(MemoryStream dout, string serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.String);
            }
            byte[] bytes = Encoding.UTF8.GetBytes(serObject);
            if (bytes.Length > 0x7fff)
            {
                throw new NotSupportedException("Strings that exceed a UTF8-encoded byte-length of 32767 (short.MaxValue) are not supported. Yours is: " + bytes.Length);
            }
            SerializeShort(dout, (short)bytes.Length, false);
            dout.Write(bytes, 0, bytes.Length);
        }

        private static void SerializeStringArray(MemoryStream dout, string[] serObject, bool setType)
        {
            if (setType)
            {
                dout.WriteByte((byte)GpType.StringArray);
            }
            SerializeShort(dout, (short)serObject.Length, false);
            for (int i = 0; i < serObject.Length; i++)
            {
                SerializeString(dout, serObject[i], false);
            }
        }

        internal static bool TryRegisterType(Type type, byte typeCode, SerializeMethod serializeFunction, DeserializeMethod deserializeFunction)
        {
            if (CodeDict.ContainsKey(typeCode) || TypeDict.ContainsKey(type))
            {
                return false;
            }
            CustomType customType = new CustomType(type, typeCode, serializeFunction, deserializeFunction);
            CodeDict.Add(typeCode, customType);
            TypeDict.Add(type, customType);
            return true;
        }

        internal static bool TryRegisterType(Type type, byte typeCode, SerializeStreamMethod serializeFunction, DeserializeStreamMethod deserializeFunction)
        {
            if (CodeDict.ContainsKey(typeCode) || TypeDict.ContainsKey(type))
            {
                return false;
            }
            CustomType customType = new CustomType(type, typeCode, serializeFunction, deserializeFunction);
            CodeDict.Add(typeCode, customType);
            TypeDict.Add(type, customType);
            return true;
        }
    }
}
