﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ExitGames.Client.Photon
{
    public abstract class IPhotonSocket
    {
        internal PeerBase peerBase;
        public bool PollReceive;
        public bool Connected =>
            this.State == PhotonSocketState.Connected;

        protected IPhotonPeerListener Listener =>
            this.peerBase.Listener;

        public int MTU =>
            this.peerBase.mtu;

        public ConnectionProtocol Protocol { get; protected set; }

        public string ServerAddress { get; protected set; }

        public int ServerPort { get; protected set; }

        public PhotonSocketState State { get; protected set; }
        public IPhotonSocket(PeerBase peerBase)
        {
            this.peerBase = peerBase ?? throw new Exception("Can't init without peer");
        }

        public virtual bool Connect()
        {
            if (this.State != PhotonSocketState.Disconnected)
            {
                if (this.peerBase.debugOut >= DebugLevel.ERROR)
                {
                    this.peerBase.Listener.DebugReturn(DebugLevel.ERROR, "Connect() failed: connection in State: " + this.State);
                }
                return false;
            }
            if (this.peerBase == null || this.Protocol != this.peerBase.usedProtocol)
            {
                return false;
            }
            if (!this.TryParseAddress(this.peerBase.ServerAddress, out string address, out ushort port))
            {
                if (this.peerBase.debugOut >= DebugLevel.ERROR)
                {
                    this.peerBase.Listener.DebugReturn(DebugLevel.ERROR, "Failed parsing address: " + this.peerBase.ServerAddress);
                }
                return false;
            }
            this.ServerAddress = address;
            this.ServerPort = port;
            return true;
        }

        public abstract bool Disconnect();

        public void EnqueueDebugReturn(DebugLevel debugLevel, string message)
        {
            this.peerBase.EnqueueDebugReturn(debugLevel, message);
        }

        protected internal static IPAddress GetIpAddress(string serverIp)
        {
            if (IPAddress.TryParse(serverIp, out IPAddress address))
            {
                return address;
            }
            IPAddress[] addressList = Dns.GetHostEntry(serverIp).AddressList;
            foreach (IPAddress address2 in addressList)
            {
                if (address2.AddressFamily == AddressFamily.InterNetwork)
                {
                    return address2;
                }
            }
            foreach (IPAddress address2 in addressList)
            {
                if (address2.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    return address2;
                }
            }
            return null;
        }

        protected internal void HandleException(StatusCode statusCode)
        {
            this.State = PhotonSocketState.Disconnecting;
            this.peerBase.EnqueueStatusCallback(statusCode);
            this.peerBase.EnqueueActionForDispatch(() => this.peerBase.Disconnect());
        }

        public void HandleReceivedDatagram(byte[] inBuffer, int length, bool willBeReused)
        {
            PeerBase.MyAction receiveAction = null;
            if (this.peerBase.NetworkSimulationSettings.IsSimulationEnabled)
            {
                if (willBeReused)
                {
                    byte[] inBufferCopy = new byte[length];
                    Buffer.BlockCopy(inBuffer, 0, inBufferCopy, 0, length);
                    this.peerBase.ReceiveNetworkSimulated(() => this.peerBase.ReceiveIncomingCommands(inBufferCopy, length));
                }
                else
                {
                    if (receiveAction == null)
                    {
                        receiveAction = () => this.peerBase.ReceiveIncomingCommands(inBuffer, length);
                    }
                    this.peerBase.ReceiveNetworkSimulated(receiveAction);
                }
            }
            else
            {
                this.peerBase.ReceiveIncomingCommands(inBuffer, length);
            }
        }

        public abstract PhotonSocketError Receive(out byte[] data);

        public bool ReportDebugOfLevel(DebugLevel levelOfMessage) =>
            (this.peerBase.debugOut >= levelOfMessage);

        public abstract PhotonSocketError Send(byte[] data, int length);
        // <summary>
        // Separates the given address into address(host name or IP) and port.Port must be included after colon!
        // </summary>
        // <remarks>

        //This method expects any address to include a port.The final ':' in addressAndPort has to separate it.
        //IPv6 addresses have multiple colons and <b>must use brackets</b> to separate address from port.


        //Examples:

        //    ns.exitgames.com:5058

        //    http://[2001:db8:1f70::999:de8:7648:6e8]:100/

        //    [2001:db8:1f70::999:de8:7648:6e8]:100

        //See:

        //    http://serverfault.com/questions/205793/how-can-one-distinguish-the-host-and-the-port-in-an-ipv6-url
        // </remarks>
        protected internal bool TryParseAddress(string addressAndPort, out string address, out ushort port)
        {
            address = string.Empty;
            port = 0;
            if (string.IsNullOrEmpty(addressAndPort))
            {
                return false;
            }
            int length = addressAndPort.LastIndexOf(':');
            if (length <= 0)
            {
                return false;
            }
            if (addressAndPort.IndexOf(':') != length && !addressAndPort.Contains("[") || !addressAndPort.Contains("]"))
            {
                return false;
            }
            address = addressAndPort.Substring(0, length);
            return ushort.TryParse(addressAndPort.Substring(length + 1), out port);
        }
    }
}
