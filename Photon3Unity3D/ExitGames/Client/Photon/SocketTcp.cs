﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security;
using System.Threading;

namespace ExitGames.Client.Photon
{
    //<summary>
    //Internal class to encapsulate the network i/o functionality for the realtime libary.
    //</summary>
    internal class SocketTcp : IPhotonSocket, IDisposable
    {
        private Socket sock;
        private readonly object syncer;

        public SocketTcp(PeerBase npeer) : base(npeer)
        {
            syncer = new object();
            if (ReportDebugOfLevel(DebugLevel.ALL))
            {
                Listener.DebugReturn(DebugLevel.ALL, "SocketTcp: TCP, DotNet, Unity.");
            }
            Protocol = ConnectionProtocol.Tcp;
            PollReceive = false;
        }

        public override bool Connect()
        {
            if (!base.Connect())
            {
                return false;
            }
            State = PhotonSocketState.Connecting;
            new Thread(new ThreadStart(DnsAndConnect))
            {
                Name = "photon dns thread",
                IsBackground = true
            }.Start();
            return true;
        }

        public override bool Disconnect()
        {
            if (ReportDebugOfLevel(DebugLevel.INFO))
            {
                EnqueueDebugReturn(DebugLevel.INFO, "SocketTcp.Disconnect()");
            }
            State = PhotonSocketState.Disconnecting;
            lock (syncer)
            {
                if (sock != null)
                {
                    try
                    {
                        sock.Close();
                    }
                    catch (Exception exception)
                    {
                        EnqueueDebugReturn(DebugLevel.INFO, "Exception in Disconnect(): " + exception);
                    }
                    sock = null;
                }
            }
            State = PhotonSocketState.Disconnected;
            return true;
        }

        public void Dispose()
        {
            State = PhotonSocketState.Disconnecting;
            if (sock != null)
            {
                try
                {
                    if (sock.Connected)
                    {
                        sock.Close();
                    }
                }
                catch (Exception exception)
                {
                    EnqueueDebugReturn(DebugLevel.INFO, "Exception in Dispose(): " + exception);
                }
            }
            sock = null;
            State = PhotonSocketState.Disconnected;
        }

        public void DnsAndConnect()
        {
            try
            {
                IPAddress ipAddress = IPhotonSocket.GetIpAddress(ServerAddress);
                if (ipAddress == null)
                {
                    throw new ArgumentException("Invalid IPAddress. Address: " + ServerAddress);
                }
                if ((ipAddress.AddressFamily != AddressFamily.InterNetwork) && (ipAddress.AddressFamily != AddressFamily.InterNetworkV6))
                {
                    throw new ArgumentException(string.Concat(new object[] { "AddressFamily '", ipAddress.AddressFamily, "' not supported. Address: ", ServerAddress }));
                }
                sock = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp)
                {
                    NoDelay = true
                };
                sock.Connect(ipAddress, ServerPort);
                State = PhotonSocketState.Connected;
            }
            catch (SecurityException exception)
            {
                if (ReportDebugOfLevel(DebugLevel.ERROR))
                {
                    Listener.DebugReturn(DebugLevel.ERROR, "Connect() to '" + ServerAddress + "' failed: " + exception.ToString());
                }
                HandleException(StatusCode.SecurityExceptionOnConnect);
                return;
            }
            catch (Exception exception2)
            {
                if (ReportDebugOfLevel(DebugLevel.ERROR))
                {
                    Listener.DebugReturn(DebugLevel.ERROR, "Connect() to '" + ServerAddress + "' failed: " + exception2.ToString());
                }
                HandleException(StatusCode.ExceptionOnConnect);
                return;
            }
            new Thread(new ThreadStart(ReceiveLoop))
            {
                Name = "photon receive thread",
                IsBackground = true
            }.Start();
        }

        public override PhotonSocketError Receive(out byte[] data)
        {
            data = null;
            return PhotonSocketError.NoData;
        }

        public void ReceiveLoop()
        {
            MemoryStream stream = new MemoryStream(MTU);
            while (State == PhotonSocketState.Connected)
            {
                stream.Position = 0L;
                stream.SetLength(0L);
                try
                {
                    int offset = 0;
                    int num2 = 0;
                    byte[] buffer = new byte[9];
                    while (offset < 9)
                    {
                        num2 = this.sock.Receive(buffer, offset, 9 - offset, SocketFlags.None);
                        offset += num2;
                        if (num2 == 0)
                        {
                            throw new SocketException((int)SocketError.ConnectionReset);
                        }
                    }
                    if (buffer[0] == 240)
                    {
                        HandleReceivedDatagram(buffer, buffer.Length, false);
                        continue;
                    }
                    int size = (((buffer[1] << 0x18) | (buffer[2] << 0x10)) | (buffer[3] << 8)) | buffer[4];
                    if (peerBase.TrafficStatsEnabled)
                    {
                        if (buffer[5] == 0)
                        {
                            peerBase.TrafficStatsIncoming.CountReliableOpCommand(size);
                        }
                        else
                        {
                            peerBase.TrafficStatsIncoming.CountUnreliableOpCommand(size);
                        }
                    }
                    if (ReportDebugOfLevel(DebugLevel.ALL))
                    {
                        EnqueueDebugReturn(DebugLevel.ALL, "message length: " + size);
                    }
                    stream.Write(buffer, 7, offset - 7);
                    offset = 0;
                    size -= 9;
                    buffer = new byte[size];
                    while (offset < size)
                    {
                        num2 = sock.Receive(buffer, offset, size - offset, SocketFlags.None);
                        offset += num2;
                        if (num2 == 0)
                        {
                            throw new SocketException((int)SocketError.ConnectionReset);
                        }
                    }
                    stream.Write(buffer, 0, offset);
                    if (stream.Length > 0L)
                    {
                        HandleReceivedDatagram(stream.ToArray(), (int)stream.Length, false);
                    }
                    if (ReportDebugOfLevel(DebugLevel.ALL))
                    {
                        EnqueueDebugReturn(DebugLevel.ALL, "TCP < " + stream.Length + ((stream.Length == (size + 2)) ? " OK" : " BAD"));
                    }
                }
                catch (SocketException exception)
                {
                    if (State != PhotonSocketState.Disconnecting && State != PhotonSocketState.Disconnected)
                    {
                        if (ReportDebugOfLevel(DebugLevel.ERROR))
                        {
                            EnqueueDebugReturn(DebugLevel.ERROR, "Receiving failed. SocketException: " + exception.SocketErrorCode);
                        }
                        if (exception.SocketErrorCode == SocketError.ConnectionReset || exception.SocketErrorCode == SocketError.ConnectionAborted)
                        {
                            HandleException(StatusCode.DisconnectByServer);
                        }
                        else
                        {
                            HandleException(StatusCode.ExceptionOnReceive);
                        }
                    }
                }
                catch (Exception exception2)
                {
                    if (State != PhotonSocketState.Disconnecting && State != PhotonSocketState.Disconnected)
                    {
                        if (ReportDebugOfLevel(DebugLevel.ERROR))
                        {
                            EnqueueDebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Receive issue. State: ", State, ". Server: '", ServerAddress, "' Exception: ", exception2 }));
                        }
                        HandleException(StatusCode.ExceptionOnReceive);
                    }
                }
            }
            Disconnect();
        }
        //<summary>
        //used by TPeer*
        //</summary>
        public override PhotonSocketError Send(byte[] data, int length)
        {
            if (!sock.Connected)
            {
                return PhotonSocketError.Skipped;
            }
            try
            {
                sock.Send(data);
            }
            catch (Exception exception)
            {
                if (ReportDebugOfLevel(DebugLevel.ERROR))
                {
                    EnqueueDebugReturn(DebugLevel.ERROR, "Cannot send to: " + ServerAddress + ". " + exception.Message);
                }
                HandleException(StatusCode.Exception);
                return PhotonSocketError.Exception;
            }
            return PhotonSocketError.Success;
        }
    }
}