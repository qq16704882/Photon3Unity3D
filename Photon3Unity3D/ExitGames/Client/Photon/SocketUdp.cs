﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security;
using System.Threading;

namespace ExitGames.Client.Photon
{
    //<summary> Internal class to encapsulate the network i/o functionality for the realtime libary.</summary>
    internal class SocketUdp : IPhotonSocket, IDisposable
    {
        private Socket sock;

        private readonly object syncer;

        public SocketUdp(PeerBase npeer) : base(npeer)
        {
            this.syncer = new object();
            if (base.ReportDebugOfLevel(DebugLevel.ALL))
            {
                base.Listener.DebugReturn(DebugLevel.ALL, "CSharpSocket: UDP, Unity3d.");
            }
            base.Protocol = ConnectionProtocol.Udp;
            base.PollReceive = false;
        }

        public override bool Connect()
        {
            lock (this.syncer)
            {
                if (!base.Connect())
                {
                    return false;
                }
                base.State = PhotonSocketState.Connecting;
                new Thread(new ThreadStart(this.DnsAndConnect))
                {
                    Name = "photon dns thread",
                    IsBackground = true
                }.Start();
                return true;
            }
        }

        public override bool Disconnect()
        {
            if (base.ReportDebugOfLevel(DebugLevel.INFO))
            {
                base.EnqueueDebugReturn(DebugLevel.INFO, "CSharpSocket.Disconnect()");
            }
            base.State = PhotonSocketState.Disconnecting;
            lock (this.syncer)
            {
                if (this.sock != null)
                {
                    try
                    {
                        this.sock.Close();
                    }
                    catch (Exception exception)
                    {
                        base.EnqueueDebugReturn(DebugLevel.INFO, "Exception in Disconnect(): " + exception);
                    }
                    this.sock = null;
                }
            }
            base.State = PhotonSocketState.Disconnected;
            return true;
        }

        public void Dispose()
        {
            base.State = PhotonSocketState.Disconnecting;
            if (this.sock != null)
            {
                try
                {
                    if (this.sock.Connected)
                    {
                        this.sock.Close();
                    }
                }
                catch (Exception exception)
                {
                    base.EnqueueDebugReturn(DebugLevel.INFO, "Exception in Dispose(): " + exception);
                }
            }
            this.sock = null;
            base.State = PhotonSocketState.Disconnected;
        }

        internal void DnsAndConnect()
        {
            try
            {
                lock (this.syncer)
                {
                    IPAddress ipAddress = IPhotonSocket.GetIpAddress(base.ServerAddress);
                    if (ipAddress == null)
                    {
                        throw new ArgumentException("Invalid IPAddress. Address: " + base.ServerAddress);
                    }
                    if ((ipAddress.AddressFamily != AddressFamily.InterNetwork) && (ipAddress.AddressFamily != AddressFamily.InterNetworkV6))
                    {
                        throw new ArgumentException(string.Concat(new object[] { "AddressFamily '", ipAddress.AddressFamily, "' not supported. Address: ", base.ServerAddress }));
                    }
                    this.sock = new Socket(ipAddress.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                    this.sock.Connect(ipAddress, base.ServerPort);
                    base.State = PhotonSocketState.Connected;
                }
            }
            catch (SecurityException exception)
            {
                if (base.ReportDebugOfLevel(DebugLevel.ERROR))
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, "Connect() to '" + base.ServerAddress + "' failed: " + exception.ToString());
                }
                base.HandleException(StatusCode.SecurityExceptionOnConnect);
                return;
            }
            catch (Exception exception2)
            {
                if (base.ReportDebugOfLevel(DebugLevel.ERROR))
                {
                    base.Listener.DebugReturn(DebugLevel.ERROR, "Connect() to '" + base.ServerAddress + "' failed: " + exception2.ToString());
                }
                base.HandleException(StatusCode.ExceptionOnConnect);
                return;
            }
            new Thread(new ThreadStart(this.ReceiveLoop))
            {
                Name = "photon receive thread",
                IsBackground = true
            }.Start();
        }

        public override PhotonSocketError Receive(out byte[] data)
        {
            data = null;
            return PhotonSocketError.NoData;
        }
        //<summary>Endless loop, run in Receive Thread.</summary>
        public void ReceiveLoop()
        {
            byte[] buffer = new byte[base.MTU];
            while (base.State == PhotonSocketState.Connected)
            {
                try
                {
                    int length = this.sock.Receive(buffer);
                    base.HandleReceivedDatagram(buffer, length, true);
                }
                catch (Exception exception)
                {
                    if ((base.State != PhotonSocketState.Disconnecting) && (base.State != PhotonSocketState.Disconnected))
                    {
                        if (base.ReportDebugOfLevel(DebugLevel.ERROR))
                        {
                            base.EnqueueDebugReturn(DebugLevel.ERROR, string.Concat(new object[] { "Receive issue. State: ", base.State, ". Server: '", base.ServerAddress, "' Exception: ", exception }));
                        }
                        base.HandleException(StatusCode.ExceptionOnReceive);
                    }
                }
            }
            this.Disconnect();
        }
        //<summary>used by PhotonPeer*</summary>
        public override PhotonSocketError Send(byte[] data, int length)
        {
            lock (this.syncer)
            {
                if (!((this.sock != null) && this.sock.Connected))
                {
                    return PhotonSocketError.Skipped;
                }
                try
                {
                    this.sock.Send(data, 0, length, SocketFlags.None);
                }
                catch (Exception exception)
                {
                    if (base.ReportDebugOfLevel(DebugLevel.ERROR))
                    {
                        base.EnqueueDebugReturn(DebugLevel.ERROR, "Cannot send to: " + base.ServerAddress + ". " + exception.Message);
                    }
                    return PhotonSocketError.Exception;
                }
            }
            return PhotonSocketError.Success;
        }
    }
}