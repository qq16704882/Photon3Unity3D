﻿using System;
using System.IO;

namespace ExitGames.Client.Photon
{
    //<summary>
    //Type of serialization methods to add custom type support.
    //Use PhotonPeer.ReisterType() to register new types with serialization and deserialization methods.
    //</summary>
    //<param name = "customObject" > The method will get objects passed that were registered with it in RegisterType().</param>
    //<returns>Return a byte[] that resembles the object passed in. The framework will surround it with length and type info, so don't include it.</returns>
    public delegate byte[] SerializeMethod(object customObject);
    //<summary>
    //Type of deserialization methods to add custom type support.
    //Use PhotonPeer.RegisterType() to register new types with serialization and deserialization methods.
    //</summary>
    //<param name = "serializedCustomObject" > The framwork passes in the data it got by the associated SerializeMethod. The type code and length are stripped and applied before a DeserializeMethod is called.</param>
    //<returns>Return a object of the type that was associated with this method through RegisterType().</returns>
    public delegate object DeserializeMethod(byte[] serializedCustomObject);
    
    public delegate short SerializeStreamMethod(MemoryStream outStream, object customObject);

    public delegate object DeserializeStreamMethod(MemoryStream inStream, short length);

    internal class CustomType
    {
        public readonly byte Code;
        public readonly DeserializeMethod DeserializeFunction;
        public readonly DeserializeStreamMethod DeserializeStreamFunction;
        public readonly SerializeMethod SerializeFunction;
        public readonly SerializeStreamMethod SerializeStreamFunction;
        public readonly Type Type;

        public CustomType(Type type, byte code, SerializeMethod serializeFunction, DeserializeMethod deserializeFunction)
        {
            this.Type = type;
            this.Code = code;
            this.SerializeFunction = serializeFunction;
            this.DeserializeFunction = deserializeFunction;
        }

        public CustomType(Type type, byte code, SerializeStreamMethod serializeFunction, DeserializeStreamMethod deserializeFunction)
        {
            this.Type = type;
            this.Code = code;
            this.SerializeStreamFunction = serializeFunction;
            this.DeserializeStreamFunction = deserializeFunction;
        }
    }
}
