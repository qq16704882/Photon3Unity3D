﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExitGames.Client.Photon
{
    public enum PhotonDisconnectCause
    {
        SecurityExceptionOnConnect,
        ExceptionOnConnect,
        Exception,
        ReadException,
        WriteException
    }
}
