﻿using Photon.SocketServer.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace ExitGames.Client.Photon
{
    public abstract class PeerBase
    {
        internal readonly Queue<MyAction> ActionQueue = new Queue<MyAction>();
        internal bool ApplicationIsInitialized;
        public int ByteCountCurrentDispatch;
        public int ByteCountLastOperation;
        internal long bytesIn;
        internal long bytesOut;
        internal byte ChannelCount = 2;
        public const string ClientVersion = "4.0.5.0";
        internal int commandBufferSize = 100;
        internal NCommand CommandInCurrentDispatch;
        //<summary>Log of sent reliable commands and incoming ACKs.</summary>
        internal Queue<CmdLogItem> CommandLog;
        //<summary>Size of CommandLog. Default is 0, no logging.</summary>
        internal int CommandLogSize = 0;
        internal bool crcEnabled = false;
        internal DiffieHellmanCryptoProvider CryptoProvider;
        internal DebugLevel debugOut = DebugLevel.ERROR;
        internal int DisconnectTimeout = 0x2710;//10000
        internal const int ENET_PEER_DEFAULT_ROUND_TRIP_TIME = 300;
        internal const int ENET_PEER_PACKET_LOSS_SCALE = 0x10000;//65536
        internal const int ENET_PEER_PACKET_THROTTLE_INTERVAL = 0x1388;//5000
        internal int highestRoundTripTimeVariance;
        internal byte[] INIT_BYTES = new byte[0x29];//41
        //<summary>Log of incoming reliable commands, used to track which commands from the server this client got. Part of the PhotonPeer.CommandLogToString() result.</summary>
        internal Queue<CmdLogItem> InReliableLog;
        internal bool isEncryptionAvailable;
        private readonly Random lagRandomizer = new Random();
        internal int lastRoundTripTime;
        internal int lastRoundTripTimeVariance;
        internal int limitOfUnreliableCommands = 0;
        internal int lowestRoundTripTime;
        //<summary> Maximum Transfer Unit to be used for UDP+TCP</summary>
        internal int mtu = 0x4b0;//1200
        internal readonly LinkedList<SimulationItem> NetSimListIncoming = new LinkedList<SimulationItem>();
        internal readonly LinkedList<SimulationItem> NetSimListOutgoing = new LinkedList<SimulationItem>();
        private readonly NetworkSimulationSet networkSimulationSettings = new NetworkSimulationSet();
        internal int outgoingCommandsInStream = 0;
        internal static int outgoingStreamBufferSize = 0x4b0;//1200
        internal int packetLossByChallenge;
        internal int packetLossByCrc;
        internal int packetThrottleInterval;
        //  <summary>
        //  This is the(low level) connection state of the peer.It's internal and based on eNet's states.

        //</summary>
        //  <remarks>Applications can read the "high level" state as PhotonPeer.PeerState, which uses a different enum.</remarks>
        internal ConnectionStateValue peerConnectionState;
        internal static short peerCount;
        //This ID is assigned by the Realtime Server upon connection.
        //The application does not have to care about this, but it is useful in debugging.
        internal short peerID = -1;
        //<summary> (default=6) Rhttp: maximum number of open connections, should be &gt; rhttpMinConnections </summary>
        internal int rhttpMaxConnections = 6;
        //<summary> (default=2) Rhttp: minimum number of open connections </summary>
        internal int rhttpMinConnections = 2;
        internal int roundTripTime;
        internal int roundTripTimeVariance;
        internal IPhotonSocket rt;
        internal int sentCountAllowance = 5;
        protected MemoryStream SerializeMemStream = new MemoryStream();
        // <summary>
        // The serverTimeOffset is serverTimestamp - localTime.Used to approximate the serverTimestamp with help of localTime
        //</summary>
        internal int serverTimeOffset;
        internal bool serverTimeOffsetIsAvailable;
        protected internal Type SocketImplementation = null;
        internal int timeBase;
        internal int timeInt;
        internal int timeLastAckReceive;
        internal int timeLastSendAck;
        //<summary>Set to timeInt, whenever SendOutgoingCommands actually checks outgoing queues to send them. Must be connected.</summary>
        internal int timeLastSendOutgoing;
        internal int timeoutInt;
        internal int timePingInterval = 0x3e8;//1000
        internal int timestampOfLastReceive;
        internal int TrafficPackageHeaderSize;
        private bool trafficStatsEnabled = false;
        public TrafficStatsGameLevel TrafficStatsGameLevel;
        public TrafficStats TrafficStatsIncoming;
        public TrafficStats TrafficStatsOutgoing;
        private Stopwatch trafficStatsStopwatch;
        internal ConnectionProtocol usedProtocol;
        internal int warningSize = 100;

        protected PeerBase()
        {
        }
        //<summary>Initializes the CommandLog and InReliableLog according to CommandLogSize. A value of 0 will set both logs to 0.</summary>
        internal void CommandLogInit()
        {
            if (this.CommandLogSize <= 0)
            {
                this.CommandLog = null;
                this.InReliableLog = null;
            }
            else if (this.CommandLog == null || this.InReliableLog == null)
            {
                this.CommandLog = new Queue<CmdLogItem>(this.CommandLogSize);
                this.InReliableLog = new Queue<CmdLogItem>(this.CommandLogSize);
            }
            else
            {
                this.CommandLog.Clear();
                this.InReliableLog.Clear();
            }
        }
        //<summary>Reduce CommandLog to CommandLogSize. Oldest entries get discarded.</summary>
        internal void CommandLogResize()
        {
            if (this.CommandLogSize <= 0)
            {
                this.CommandLog = null;
                this.InReliableLog = null;
            }
            else
            {
                if (this.CommandLog == null || this.InReliableLog == null)
                {
                    this.CommandLogInit();
                }
                while (this.CommandLog.Count > 0 && this.CommandLog.Count > this.CommandLogSize)
                {
                    this.CommandLog.Dequeue();
                }
                while (this.InReliableLog.Count > 0 && this.InReliableLog.Count > this.CommandLogSize)
                {
                    this.InReliableLog.Dequeue();
                }
            }
        }
        //<summary>Converts the CommandLog into a readable table-like string with summary.</summary>
        public string CommandLogToString()
        {
            StringBuilder builder = new StringBuilder();
            int num = (this.usedProtocol != ConnectionProtocol.Udp) ? 0 : ((EnetPeer)this).reliableCommandsRepeated;
            builder.AppendFormat("PeerId: {0} Now: {1} Server: {2} State: {3} Total Resends: {4} Received {5}ms ago.\n", new object[] { this.PeerID, this.timeInt, this.ServerAddress, this.peerConnectionState, num, SupportClass.GetTickCount() - this.timestampOfLastReceive });
            if (this.CommandLog != null)
            {
                foreach (CmdLogItem item in this.CommandLog)
                {
                    builder.AppendLine(item.ToString());
                }
                builder.AppendLine("Received Reliable Log: ");
                foreach (CmdLogItem item in this.InReliableLog)
                {
                    builder.AppendLine(item.ToString());
                }
            }
            return builder.ToString();
        }
        //<summary>Connect to server and send Init (which inlcudes the appId).</summary>
        internal abstract bool Connect(string serverAddress, string appID);
        internal void DeriveSharedKey(OperationResponse operationResponse)
        {
            if (operationResponse.ReturnCode != 0)
            {
                this.EnqueueDebugReturn(DebugLevel.ERROR, "Establishing encryption keys failed. " + operationResponse.ToStringFull());
                this.EnqueueStatusCallback(StatusCode.EncryptionFailedToEstablish);
            }
            else
            {
                byte[] otherPartyPublicKey = (byte[])operationResponse[PhotonCodes.ServerKey];
                if (otherPartyPublicKey == null || otherPartyPublicKey.Length == 0)
                {
                    this.EnqueueDebugReturn(DebugLevel.ERROR, "Establishing encryption keys failed. Server's public key is null or empty. " + operationResponse.ToStringFull());
                    this.EnqueueStatusCallback(StatusCode.EncryptionFailedToEstablish);
                }
                else
                {
                    this.CryptoProvider.DeriveSharedKey(otherPartyPublicKey);
                    this.isEncryptionAvailable = true;
                    this.EnqueueStatusCallback(StatusCode.EncryptionEstablished);
                }
            }
        }

        internal virtual bool DeserializeMessageAndCallback(byte[] inBuff)
        {
            OperationResponse response;
            if (inBuff.Length < 2)
            {
                if (this.debugOut >= DebugLevel.ERROR)
                {
                    this.Listener.DebugReturn(DebugLevel.ERROR, "Incoming UDP data too short! " + inBuff.Length);
                }
                return false;
            }
            if (inBuff[0] != 0xf3 && inBuff[0] != 0xfd)
            {
                if (this.debugOut >= DebugLevel.ERROR)
                {
                    this.Listener.DebugReturn(DebugLevel.ALL, "No regular operation UDP message: " + inBuff[0]);
                }
                return false;
            }
            byte num = (byte)(inBuff[1] & 0x7f);
            bool flag = (inBuff[1] & 0x80) > 0;
            MemoryStream memoryStream = null;
            if (num != 1)
            {
                try
                {
                    if (flag)
                    {
                        inBuff = this.CryptoProvider.Decrypt(inBuff, 2, inBuff.Length - 2);
                        memoryStream = new MemoryStream(inBuff);
                    }
                    else
                    {
                        memoryStream = new MemoryStream(inBuff);
                        memoryStream.Seek(2L, SeekOrigin.Begin);
                    }
                }
                catch (Exception exception)
                {
                    if (this.debugOut >= DebugLevel.ERROR)
                    {
                        this.Listener.DebugReturn(DebugLevel.ERROR, exception.ToString());
                    }
                    SupportClass.WriteStackTrace(exception);
                    return false;
                }
            }
            int tickCount = 0;
            switch (num)
            {
                case 1:
                    this.InitCallback();
                    return true;//goto Label_0363;

                case 3:
                    response = Protocol.DeserializeOperationResponse(memoryStream);
                    if (this.TrafficStatsEnabled)
                    {
                        this.TrafficStatsGameLevel.CountResult(this.ByteCountCurrentDispatch);
                        tickCount = SupportClass.GetTickCount();
                    }
                    this.Listener.OnOperationResponse(response);
                    if (this.TrafficStatsEnabled)
                    {
                        this.TrafficStatsGameLevel.TimeForResponseCallback(response.OperationCode, SupportClass.GetTickCount() - tickCount);
                    }
                    return true; // goto Label_0363;

                case 4:
                    {
                        EventData eventData = Protocol.DeserializeEventData(memoryStream);
                        if (this.TrafficStatsEnabled)
                        {
                            this.TrafficStatsGameLevel.CountEvent(this.ByteCountCurrentDispatch);
                            tickCount = SupportClass.GetTickCount();
                        }
                        this.Listener.OnEvent(eventData);
                        if (this.TrafficStatsEnabled)
                        {
                            this.TrafficStatsGameLevel.TimeForEventCallback(eventData.Code, SupportClass.GetTickCount() - tickCount);
                        }
                        return true;// goto Label_0363;
                    }
                case 7:
                    response = Protocol.DeserializeOperationResponse(memoryStream);
                    if (this.TrafficStatsEnabled)
                    {
                        this.TrafficStatsGameLevel.CountResult(this.ByteCountCurrentDispatch);
                        tickCount = SupportClass.GetTickCount();
                    }
                    if (response.OperationCode == PhotonCodes.InitEncryption)
                    {
                        this.DeriveSharedKey(response);
                    }
                    else
                    {
                        if (response.OperationCode != PhotonCodes.Ping)
                        {
                            this.EnqueueDebugReturn(DebugLevel.ERROR, "Received unknown internal operation. " + response.ToStringFull());
                            break;
                        }
                        TPeer peer = this as TPeer;
                        if (peer != null)
                        {
                            peer.ReadPingResult(response);
                        }
                        else
                        {
                            this.EnqueueDebugReturn(DebugLevel.ERROR, "Ping response not used. " + response.ToStringFull());
                        }
                    }
                    break;

                default:
                    this.EnqueueDebugReturn(DebugLevel.ERROR, "unexpected msgType " + num);
                    return true;// goto Label_0363;
            }
            if (this.TrafficStatsEnabled)
            {
                this.TrafficStatsGameLevel.TimeForResponseCallback(response.OperationCode, SupportClass.GetTickCount() - tickCount);
            }
            // Label_0363:
            return true;
        }

        internal abstract void Disconnect();
        //<summary>
        //Checks the incoming queue and Dispatches received data if possible.
        //</summary>
        //<returns>If a Dispatch happened or not, which shows if more Dispatches might be needed.</returns>
        internal abstract bool DispatchIncomingCommands();
        internal void EnqueueActionForDispatch(MyAction action)
        {
            lock (this.ActionQueue)
            {
                this.ActionQueue.Enqueue(action);
            }
        }

        internal void EnqueueDebugReturn(DebugLevel level, string debugReturn)
        {
            MyAction item = null;
            lock (this.ActionQueue)
            {
                if (item == null)
                {
                    item = () => this.Listener.DebugReturn(level, debugReturn);
                }
                this.ActionQueue.Enqueue(item);
            }
        }

        internal bool EnqueueOperation(Dictionary<byte, object> parameters, byte opCode, bool sendReliable, byte channelId, bool encrypted) =>
            this.EnqueueOperation(parameters, opCode, sendReliable, channelId, encrypted, EgMessageType.Operation);

        internal abstract bool EnqueueOperation(Dictionary<byte, object> parameters, byte opCode, bool sendReliable, byte channelId, bool encrypted, EgMessageType messageType);
        internal void EnqueueStatusCallback(StatusCode statusValue)
        {
            MyAction item = null;
            lock (this.ActionQueue)
            {
                if (item == null)
                {
                    item = () => this.Listener.OnStatusChanged(statusValue);
                }
                this.ActionQueue.Enqueue(item);
            }
        }
        //<summary>
        //Internally uses an operation to exchange encryption keys with the server.
        //</summary>
        //<returns>If the op could be sent.</returns>
        internal bool ExchangeKeysForEncryption()
        {
            this.isEncryptionAvailable = false;
            this.CryptoProvider = new DiffieHellmanCryptoProvider();
            Dictionary<byte, object> parameters = new Dictionary<byte, object>(1)
            {
                [PhotonCodes.ClientKey] = this.CryptoProvider.PublicKey
            };
            return this.EnqueueOperation(parameters, PhotonCodes.InitEncryption, true, 0, false, EgMessageType.InternalOperationRequest);
        }

        internal abstract void FetchServerTimestamp();
        private string GetHttpKeyValueString(Dictionary<string, string> dic)
        {
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in dic)
            {
                StringBuilder introduced5 = builder.Append(pair.Key).Append("=");
                introduced5.Append(pair.Value).Append("&");
            }
            return builder.ToString();
        }

        internal void InitCallback()
        {
            if (this.peerConnectionState == ConnectionStateValue.Connecting)
            {
                this.peerConnectionState = ConnectionStateValue.Connected;
            }
            this.ApplicationIsInitialized = true;
            this.FetchServerTimestamp();
            this.Listener.OnStatusChanged(StatusCode.Connect);
        }

        internal void InitializeTrafficStats()
        {
            this.TrafficStatsIncoming = new TrafficStats(this.TrafficPackageHeaderSize);
            this.TrafficStatsOutgoing = new TrafficStats(this.TrafficPackageHeaderSize);
            this.TrafficStatsGameLevel = new TrafficStatsGameLevel();
            this.trafficStatsStopwatch = new Stopwatch();
        }

        internal void InitOnce()
        {
            this.networkSimulationSettings.peerBase = this;
            this.INIT_BYTES[0] = 0xf3;
            this.INIT_BYTES[1] = 0;
            this.INIT_BYTES[2] = 1;
            this.INIT_BYTES[3] = 6;
            this.INIT_BYTES[4] = 1;
            this.INIT_BYTES[5] = 4;
            this.INIT_BYTES[6] = 0;
            this.INIT_BYTES[7] = 5;
            this.INIT_BYTES[8] = 7;
        }

        internal virtual void InitPeerBase()
        {
            LinkedList<SimulationItem> list;
            this.TrafficStatsIncoming = new TrafficStats(this.TrafficPackageHeaderSize);
            this.TrafficStatsOutgoing = new TrafficStats(this.TrafficPackageHeaderSize);
            this.TrafficStatsGameLevel = new TrafficStatsGameLevel();
            this.ByteCountLastOperation = 0;
            this.ByteCountCurrentDispatch = 0;
            this.bytesIn = 0L;
            this.bytesOut = 0L;
            this.packetLossByCrc = 0;
            this.packetLossByChallenge = 0;
            this.networkSimulationSettings.LostPackagesIn = 0;
            this.networkSimulationSettings.LostPackagesOut = 0;
            lock ((list = this.NetSimListOutgoing))
            {
                this.NetSimListOutgoing.Clear();
            }
            lock ((list = this.NetSimListIncoming))
            {
                this.NetSimListIncoming.Clear();
            }
            this.peerConnectionState = ConnectionStateValue.Disconnected;
            this.timeBase = SupportClass.GetTickCount();
            this.isEncryptionAvailable = false;
            this.ApplicationIsInitialized = false;
            this.roundTripTime = 300;
            this.roundTripTimeVariance = 0;
            this.packetThrottleInterval = 0x1388;
            this.serverTimeOffsetIsAvailable = false;
            this.serverTimeOffset = 0;
        }
        //<summary>
        //Core of the Network Simulation, which is available in Debug builds.
        //Called by a timer in intervals.
        //</summary>
        protected internal void NetworkSimRun()
        {
            SimulationItem item;
            bool flag2;
            LinkedList<SimulationItem> list;
            goto Label_0157;
            Label_00CF:
            Monitor.Enter(list = this.NetSimListOutgoing);
            try
            {
                item = null;
                while (this.NetSimListOutgoing.First != null)
                {
                    item = this.NetSimListOutgoing.First.Value;
                    if (item.stopw.ElapsedMilliseconds < item.Delay)
                    {
                        goto Label_014E;
                    }
                    item.ActionToExecute();
                    this.NetSimListOutgoing.RemoveFirst();
                }
            }
            finally
            {
                Monitor.Exit(list);
            }
            Label_014E:
            Thread.Sleep(0);
            Label_0157:
            flag2 = true;
            bool isSimulationEnabled = false;
            lock (this.networkSimulationSettings.NetSimManualResetEvent)
            {
                isSimulationEnabled = this.networkSimulationSettings.IsSimulationEnabled;
            }
            if (!isSimulationEnabled)
            {
                this.networkSimulationSettings.NetSimManualResetEvent.WaitOne();
                goto Label_0157;
            }
            lock ((list = this.NetSimListIncoming))
            {
                item = null;
                while (this.NetSimListIncoming.First != null)
                {
                    item = this.NetSimListIncoming.First.Value;
                    if (item.stopw.ElapsedMilliseconds < item.Delay)
                    {
                        goto Label_00CF;
                    }
                    item.ActionToExecute();
                    this.NetSimListIncoming.RemoveFirst();
                }
            }
            goto Label_00CF;
        }

        internal abstract void ReceiveIncomingCommands(byte[] inBuff, int dataLength);
        internal void ReceiveNetworkSimulated(MyAction receiveAction)
        {
            if (!this.networkSimulationSettings.IsSimulationEnabled)
            {
                receiveAction();
            }
            else if ((this.usedProtocol == ConnectionProtocol.Udp && this.networkSimulationSettings.IncomingLossPercentage > 0) && this.lagRandomizer.Next(0x65) < this.networkSimulationSettings.IncomingLossPercentage)
            {
                this.networkSimulationSettings.LostPackagesIn++;
            }
            else
            {
                int num = (this.networkSimulationSettings.IncomingJitter <= 0) ? 0 : (this.lagRandomizer.Next(this.networkSimulationSettings.IncomingJitter * 2) - this.networkSimulationSettings.IncomingJitter);
                int num2 = this.networkSimulationSettings.IncomingLag + num;
                int num3 = SupportClass.GetTickCount() + num2;
                SimulationItem item = new SimulationItem
                {
                    ActionToExecute = receiveAction,
                    TimeToExecute = num3,
                    Delay = num2
                };
                lock (this.NetSimListIncoming)
                {
                    if (this.NetSimListIncoming.Count == 0 || this.usedProtocol == ConnectionProtocol.Tcp)
                    {
                        this.NetSimListIncoming.AddLast(item);
                    }
                    else
                    {
                        LinkedListNode<SimulationItem> first = this.NetSimListIncoming.First;
                        while (first != null && first.Value.TimeToExecute < num3)
                        {
                            first = first.Next;
                        }
                        if (first == null)
                        {
                            this.NetSimListIncoming.AddLast(item);
                        }
                        else
                        {
                            this.NetSimListIncoming.AddBefore(first, item);
                        }
                    }
                }
            }
        }

        internal virtual bool SendAcksOnly() =>
            false;

        internal void SendNetworkSimulated(MyAction sendAction)
        {
            if (!this.NetworkSimulationSettings.IsSimulationEnabled)
            {
                sendAction();
            }
            else if ((this.usedProtocol == ConnectionProtocol.Udp && this.NetworkSimulationSettings.OutgoingLossPercentage > 0) && this.lagRandomizer.Next(0x65) < this.NetworkSimulationSettings.OutgoingLossPercentage)
            {
                this.networkSimulationSettings.LostPackagesOut++;
            }
            else
            {
                int num = (this.networkSimulationSettings.OutgoingJitter <= 0) ? 0 : (this.lagRandomizer.Next(this.networkSimulationSettings.OutgoingJitter * 2) - this.networkSimulationSettings.OutgoingJitter);
                int num2 = this.networkSimulationSettings.OutgoingLag + num;
                int num3 = SupportClass.GetTickCount() + num2;
                SimulationItem item = new SimulationItem
                {
                    ActionToExecute = sendAction,
                    TimeToExecute = num3,
                    Delay = num2
                };
                lock (this.NetSimListOutgoing)
                {
                    if (this.NetSimListOutgoing.Count == 0 || this.usedProtocol == ConnectionProtocol.Tcp)
                    {
                        this.NetSimListOutgoing.AddLast(item);
                    }
                    else
                    {
                        LinkedListNode<SimulationItem> first = this.NetSimListOutgoing.First;
                        while (first != null && first.Value.TimeToExecute < num3)
                        {
                            first = first.Next;
                        }
                        if (first == null)
                        {
                            this.NetSimListOutgoing.AddLast(item);
                        }
                        else
                        {
                            this.NetSimListOutgoing.AddBefore(first, item);
                        }
                    }
                }
            }
        }
        //<summary>
        //Checks outgoing queues for commands to send and puts them on their way.
        //This creates one package per go in UDP.
        //</summary>
        //<returns>If commands are not sent, cause they didn't fit into the package that's sent.</returns>
        internal abstract bool SendOutgoingCommands();
        //<summary> Returns the UDP Payload starting with Magic Number for binary protocol</summary>
        internal byte[] SerializeMessageToMessage(object message, bool encrypt, byte[] messageHeader, bool writeLength = true)
        {
            byte[] buffer;
            lock (this.SerializeMemStream)
            {
                this.SerializeMemStream.Position = 0L;
                this.SerializeMemStream.SetLength(0L);
                if (!encrypt)
                {
                    this.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                }
                Protocol.SerializeMessage(this.SerializeMemStream, message);
                if (encrypt)
                {
                    byte[] data = this.SerializeMemStream.ToArray();
                    data = this.CryptoProvider.Encrypt(data);
                    this.SerializeMemStream.Position = 0L;
                    this.SerializeMemStream.SetLength(0L);
                    this.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                    this.SerializeMemStream.Write(data, 0, data.Length);
                }
                buffer = this.SerializeMemStream.ToArray();
            }
            buffer[messageHeader.Length - 1] = 8;
            if (encrypt)
            {
                buffer[messageHeader.Length - 1] = (byte)(buffer[messageHeader.Length - 1] | 0x80);
            }
            if (writeLength)
            {
                int targetOffset = 1;
                Protocol.Serialize(buffer.Length, buffer, ref targetOffset);
            }
            return buffer;
        }

        internal abstract byte[] SerializeOperationToMessage(byte opCode, Dictionary<byte, object> parameters, EgMessageType messageType, bool encrypt);
        //<summary> Returns the UDP Payload starting with Magic Number for binary protocol </summary>
        internal byte[] SerializeRawMessageToMessage(byte[] data, bool encrypt, byte[] messageHeader, bool writeLength = true)
        {
            byte[] buffer;
            lock (this.SerializeMemStream)
            {
                this.SerializeMemStream.Position = 0L;
                this.SerializeMemStream.SetLength(0L);
                if (!encrypt)
                {
                    this.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                }
                this.SerializeMemStream.Write(data, 0, data.Length);
                if (encrypt)
                {
                    byte[] buffer2 = this.SerializeMemStream.ToArray();
                    buffer2 = this.CryptoProvider.Encrypt(buffer2);
                    this.SerializeMemStream.Position = 0L;
                    this.SerializeMemStream.SetLength(0L);
                    this.SerializeMemStream.Write(messageHeader, 0, messageHeader.Length);
                    this.SerializeMemStream.Write(buffer2, 0, buffer2.Length);
                }
                buffer = this.SerializeMemStream.ToArray();
            }
            buffer[messageHeader.Length - 1] = 9;
            if (encrypt)
            {
                buffer[messageHeader.Length - 1] = (byte)(buffer[messageHeader.Length - 1] | 0x80);
            }
            if (writeLength)
            {
                int targetOffset = 1;
                Protocol.Serialize(buffer.Length, buffer, ref targetOffset);
            }
            return buffer;
        }

        internal abstract void StopConnection();
        internal void UpdateRoundTripTimeAndVariance(int lastRoundtripTime)
        {
            if (lastRoundtripTime >= 0)
            {
                this.roundTripTimeVariance -= this.roundTripTimeVariance / 4;
                if (lastRoundtripTime >= this.roundTripTime)
                {
                    this.roundTripTime += (lastRoundtripTime - this.roundTripTime) / 8;
                    this.roundTripTimeVariance += (lastRoundtripTime - this.roundTripTime) / 4;
                }
                else
                {
                    this.roundTripTime += (lastRoundtripTime - this.roundTripTime) / 8;
                    this.roundTripTimeVariance -= (lastRoundtripTime - this.roundTripTime) / 4;
                }
                if (this.roundTripTime < this.lowestRoundTripTime)
                {
                    this.lowestRoundTripTime = this.roundTripTime;
                }
                if (this.roundTripTimeVariance > this.highestRoundTripTimeVariance)
                {
                    this.highestRoundTripTimeVariance = this.roundTripTimeVariance;
                }
            }
        }

        //<summary>
        //Count of all bytes coming in (including headers)
        //</summary>
        internal long BytesIn =>
            this.bytesIn;
        //<summary>
        //Count of all bytes going out (including headers)
        //</summary>
        internal long BytesOut =>
            this.bytesOut;

        internal string HttpUrlParameters { get; set; }

        internal bool IsSendingOnlyAcks { get; set; }

        internal IPhotonPeerListener Listener { get; set; }
        //<summary>
        //Gets the currently used settings for the built-in network simulation.
        //Please check the description of NetworkSimulationSet for more details.
        //</summary>
        public NetworkSimulationSet NetworkSimulationSettings =>
            this.networkSimulationSettings;

        public virtual string PeerID
        {
            get
            {
                ushort peerID = (ushort)this.peerID;
                return peerID.ToString();
            }
        }

        internal abstract int QueuedIncomingCommandsCount { get; }

        internal abstract int QueuedOutgoingCommandsCount { get; }

        public byte QuickResendAttempts { get; set; }

        public string ServerAddress { get; internal set; }

        protected internal byte[] TcpConnectionPrefix { get; set; }
        //<summary>
        //Enables or disables collection of statistics.
        //Setting this to true, also starts the stopwatch to measure the timespan the stats are collected.
        //</summary>
        public bool TrafficStatsEnabled
        {
            get =>
                this.trafficStatsEnabled;
            set
            {
                this.trafficStatsEnabled = value;
                if (value)
                {
                    if (this.trafficStatsStopwatch == null)
                    {
                        this.InitializeTrafficStats();
                    }
                    this.trafficStatsStopwatch.Start();
                }
                else if (this.trafficStatsStopwatch != null)
                {
                    this.trafficStatsStopwatch.Stop();
                }
            }
        }

        public long TrafficStatsEnabledTime =>
            ((this.trafficStatsStopwatch != null) ? this.trafficStatsStopwatch.ElapsedMilliseconds : 0L);

        //<summary>
        //This is the replacement for the const values used in eNet like: PS_DISCONNECTED, PS_CONNECTED, etc.
        //</summary>
        public enum ConnectionStateValue : byte
        {
            //<summary>No connection is available. Use connect.</summary>
            Disconnected = 0,
            //<summary>Establishing a connection already. The app should wait for a status callback.</summary>
            Connecting = 1,
            //<summary>
            //The low level connection with Photon is established. On connect, the library will automatically
            //send an Init package to select the application it connects to (see also PhotonPeer.Connect()).
            //When the Init is done, IPhotonPeerListener.OnStatusChanged() is called with connect.
            //</summary>
            //<remarks>Please note that calling operations is only possible after the OnStatusChanged() with StatusCode.Connect.</remarks>
            Connected = 3,
            //<summary>Connection going to be ended. Wait for status callback.</summary>
            Disconnecting = 4,
            //<summary>Acknowledging a disconnect from Photon. Wait for status callback.</summary>
            AcknowledgingDisconnect = 5,
            //<summary>Connection not properly disconnected.</summary>
            Zombie = 6
        }

        internal enum EgMessageType : byte
        {
            Init = 0,
            InitResponse = 1,
            Operation = 2,
            OperationResponse = 3,
            Event = 4,
            InternalOperationRequest = 6,
            InternalOperationResponse = 7,
            Message = 8,
            RawMessage = 9
        }

        internal delegate void MyAction();
    }
}
