﻿using System.Collections.Generic;

namespace ExitGames.Client.Photon
{
    internal class EnetChannel
    {
        internal byte ChannelNumber;
        internal Dictionary<int, NCommand> incomingReliableCommandsList;
        internal int incomingReliableSequenceNumber;
        internal Dictionary<int, NCommand> incomingUnreliableCommandsList;
        internal int incomingUnreliableSequenceNumber;
        internal Queue<NCommand> outgoingReliableCommandsList;
        internal int outgoingReliableSequenceNumber;
        internal Queue<NCommand> outgoingUnreliableCommandsList;
        internal int outgoingUnreliableSequenceNumber;

        public EnetChannel(byte channelNumber, int commandBufferSize)
        {
            this.ChannelNumber = channelNumber;
            this.incomingReliableCommandsList = new Dictionary<int, NCommand>(commandBufferSize);
            this.incomingUnreliableCommandsList = new Dictionary<int, NCommand>(commandBufferSize);
            this.outgoingReliableCommandsList = new Queue<NCommand>(commandBufferSize);
            this.outgoingUnreliableCommandsList = new Queue<NCommand>(commandBufferSize);
        }

        public void clearAll()
        {
            lock (this)
            {
                this.incomingReliableCommandsList.Clear();
                this.incomingUnreliableCommandsList.Clear();
                this.outgoingReliableCommandsList.Clear();
                this.outgoingUnreliableCommandsList.Clear();
            }
        }

        public bool ContainsReliableSequenceNumber(int reliableSequenceNumber) =>
            this.incomingReliableCommandsList.ContainsKey(reliableSequenceNumber);

        public bool ContainsUnreliableSequenceNumber(int unreliableSequenceNumber) =>
            this.incomingUnreliableCommandsList.ContainsKey(unreliableSequenceNumber);

        public NCommand FetchReliableSequenceNumber(int reliableSequenceNumber) =>
            this.incomingReliableCommandsList[reliableSequenceNumber];

        public NCommand FetchUnreliableSequenceNumber(int unreliableSequenceNumber) =>
            this.incomingUnreliableCommandsList[unreliableSequenceNumber];
    }
}